# Backoffice DSU

Highlight of this folder sub-folders:

- apihub-root - an apihub server (that provides OpenDSU services) that run within a government agency that
hosts the eGov backoffice.
  Although these services run within a government agency, the apihub-root services must be public, so that
citizens may download dapps and dapps may use DSU services from the government agency.
- api - REST services needed for a legacy eGov management application. (See backend-dsu/api/README.md for details).

## backoffice-dsu/api

This is a REST services server (written in NestJS) that integrates with OpenDSU, and is placed inside the backoffice-dsu
so that it can use (require) the OpenDSU modules.

## Developer instructions to setup a working directory.

This is an instance of https://gitlab.com/glass-project1/wallet/dsu-blueprint-workspace
adapted for eGov and has the same dependencies and setup instructions.

## Setting up the eGov Glass Wallet source code

### src/index.html

This src/index.html is ... based on


### Install and start apihub

```sh
npm install
npm run server
```

The `npm run server` will never return, and will log incoming requests to the terminal.
You should see a message like
```txt
[API-HUB-LAUNCHER] listening on port :8080 and ready to receive requests.
```
Note that the apihub always runs on port 8080. If you are already running another apihub - for example - from a glass-wallet-workspace development environment, you will encounter an error like:
```txt
[API-HUB] Error: listen EADDRINUSE: address already in use 0.0.0.0:8080
```
Simply, do not execute the last line `npm run server`, and re-use the same DSU services from glass-wallet-workspace.

### Install e build Apihub App's

With apihub running:

#### Install e build all Applications

```shell
npm run install-source
```

```shell
npm run build-source
```

#### Install e build only _eGov App_ 
To run [egov-dApp](https://gitlab.com/glass-project1/wallet/egov-dapp)
and [egovid-dApp](https://gitlab.com/glass-project1/wallet/egovid-dapp),
as they are independent repositories, it is necessary to install and build them separately.

```shell
# this will clone egov-dapp to backoffice-dsu workspace
npm run install-dapps
```

```shell
# to build egov-dapp and publish it on whatever apihub is running
npm run build-dapps
```
---

### Start REST API

```sh
cd api
# npm installed was already performed by parent octous.json
npm run start
```

The output will log If during the REST activity on the terminal.

This will start the REST API. You can test it using the Swagger UI at http://localhost:3000/borest/api

If you see an error like
```txt
[Nest] 93493  - 08/24/2022, 2:59:56 PM   ERROR [ExceptionHandler] connect ECONNREFUSED 127.0.0.1:5432
Error: connect ECONNREFUSED 127.0.0.1:5432
```
it means that you need to run a local database. See the instructions at the top-level /backoffice-sql/README.md

