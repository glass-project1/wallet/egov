import {DBModel} from "@glass-project1/db-decorators";
import {did, dsu, DSUBlueprint, enclave, environment, KeySSIGenArgs, OpenDSURepository, securityContext, walletDB} from "@glass-project1/dsu-blueprint";
import {DSU, DSUDIDMethods, KeySSI, KeySSIType} from "@glass-project1/opendsu-types";
import type {DSUEnclave, EnvironmentDefinition, DSUDatabase, DSUDid, DSUSecurityContext} from "@glass-project1/opendsu-types";
import {Err} from "@glass-project1/logging";
import {constructFromObject} from "@glass-project1/decorator-validation";

const {setOpenDSU} = require('@glass-project1/dsu-utils/src/jest-opendsu-client.js');


let testTimeout = 20000;
if(process.env["GITLAB_CI"])
    testTimeout = 50000

jest.setTimeout(testTimeout)

beforeAll(() => {
    setOpenDSU();
    // getLogger().setLevel(0)
})

@DSUBlueprint(undefined, KeySSIType.SEED)
class TestModelSeed extends DBModel {

    @did(undefined, DSUDIDMethods.SREAD)
    did?: DSUDid = undefined;

    @enclave()
    enclave?: DSUEnclave = undefined;

    @environment()
    environment?: EnvironmentDefinition = undefined;

    @walletDB()
    db?: DSUDatabase = undefined;

    @securityContext()
    sc?: DSUSecurityContext = undefined;

    constructor(model?: TestModelSeed | {}) {
        super(model);
        constructFromObject(this, model);
    }

}

@DSUBlueprint(undefined, KeySSIType.ARRAY)
class TestModelCompost extends DBModel {

    @dsu(TestModelSeed, false, true)
    wallet?: TestModelSeed

    constructor(model?: TestModelCompost | {}) {
        super();
        constructFromObject(this, model);
    }
}


describe.skip("Security Context - using in non wallet DSUS", () => {

    it("creates a compost", (callback) => {
        const repo = new OpenDSURepository(TestModelCompost, "default");

        const model = new TestModelCompost({
            wallet: {
                environment: {
                    vault: "server",
                    enclaveType: "WalletDBEnclave",
                    domain: "default",
                    didDomain: "vault",
                    vaultDomain: "vault"
                }
            }
        })

        repo.create(model, (err: Err, newModel?: TestModelSeed, dsu?: DSU, keySSI?: KeySSI) => {
            expect(err).toBeUndefined();
            expect(newModel).toBeDefined();
            expect(dsu).toBeDefined();
            expect(keySSI).toBeDefined();
            expect(keySSI?.getTypeName()).toEqual(KeySSIType.ARRAY);

            expect(newModel?.wallet).toBeDefined();
            expect(newModel?.wallet).toBeInstanceOf(TestModelSeed);
            callback();
        })
    })

})

