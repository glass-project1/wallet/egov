import {BlueprintOpenDSURepository} from '@glass-project1/dsu-blueprint';
import {
    Callback,
    DSU,
    DSUDid,
    DSUDIDMethods,
    DSUSecurityContext,
    Err,
    getSCApi,
    getW3cDIDApi,
    KeySSI,
    OpenDSUInitializationEvent
} from '@glass-project1/opendsu-types';
import {EGovDApp} from '@glass-project1/glass-toolkit/lib/blueprints/eGovernance/EGovDApp';
import {EGovId} from '@glass-project1/glass-toolkit/lib/blueprints/eGovernance/EGovId';
import {IEGovDApp, IEGovId, IEGovIdDApp, toolkitInjectables} from "@glass-project1/glass-toolkit";

const {setOpenDSU} = require('@glass-project1/dsu-utils/src/jest-opendsu-client.js');



jest.setTimeout(50000)


beforeAll(() => {
    setOpenDSU();
})


describe("EGovernance Creation", () => {

    const getEGov = function(id?: string){

        const eGovId: IEGovId = EGovId(toolkitInjectables, {
            id: "id" + (id || Math.floor(Math.random() * 10000)),
            givenName: "name",
            surname: "surname",
            issuingDate: new Date(),
            issuingLocation: "issuingLocation",
            issuingRegion: "issuingRegion",
            issuingAuthority: "issuingAuthority",
            fatherFullName: "fatherFullName",
            motherFullName: "motherFullName",
            birthDate: new Date(),
            citizenship: "citizenship",
            nationality: "nationality",
            gender: "gender",
            height: "height",
            currentAddress: "currentAddress"
        })

        const eGovDApp = EGovDApp(toolkitInjectables, {
            id: {
                id: eGovId,
                code: "egovid-dapp"
            },
            code: "egov-dapp"
        })

        return eGovDApp;
    }

    const getCreds = function (eGovDApp: IEGovDApp) {
        return {
            keyGenArgs: ["This is such a secure password!" + eGovDApp.id?.id?.id],
            childSSIArgs: [
                {
                    keyGenArgs: ["This is such a secure password again!" + eGovDApp.id?.id?.id]
                }
            ]
        }
    }

    const validateEGovDAppDSU = function (dsu: DSU, callback: Callback) {
        dsu.listMountedDSUs('/', (err, mounts: any) => {
            expect(err).toBeUndefined();
            expect(mounts).toBeDefined();
            expect(mounts.length).toEqual(3);
            callback()
        })
    }

    const validateIDDSU = function (ssi: string, callback: Callback) {

        const repo = new BlueprintOpenDSURepository(EGovId, toolkitInjectables, "default");

        repo.read(ssi, (err: Err, model?: IEGovId, dsu?: DSU, ssi?: KeySSI) => {
            expect(err).toBeUndefined();
            expect(model).toBeDefined();
            expect(dsu).toBeDefined();
            expect(ssi).toBeDefined()

            dsu?.readFile("citizenAttributes.json", (err: Err, data: any) => {
                expect(err).toBeUndefined();
                expect(data).toBeDefined();

                dsu?.readFile("createdOn", (err, createdOn) => {
                    expect(err).toBeUndefined();
                    dsu?.readFile("updatedOn", (err, updatedOn) => {
                        expect(err).toBeUndefined();
                        let modelFromFile;

                        try {
                            data = JSON.parse(data);
                            data.createdOn = JSON.parse(createdOn);
                            data.updatedOn = JSON.parse(updatedOn);
                            modelFromFile = EGovId(toolkitInjectables, data);
                        } catch (e: any) {
                            return callback(e);
                        }

                        expect(modelFromFile.hasErrors()).toBeUndefined();
                        callback()
                    })
                })
            })
        })
    }

    const validateEGovModel = function (model: IEGovDApp) {
        expect(model.db).toBeDefined()
        expect(model.did).toBeDefined()
        // expect(model.code).toBeDefined()
        expect(model.enclave).toBeDefined()
        expect(model.environment).toBeDefined()
        expect(model.id).toBeDefined()
        // expect(model.id).toBeInstanceOf(EGovIdDApp)

        const eGovIdDApp = model.id as IEGovIdDApp;

        expect(eGovIdDApp["__ssi"]).toBeDefined();

        expect(eGovIdDApp.did).toBeDefined()
        // expect(eGovIdDApp.code).toBeDefined()
        expect(eGovIdDApp.environment).toBeDefined()
        expect(eGovIdDApp.id).toBeDefined();
        // expect(eGovIdDApp.id).toBeInstanceOf(EGovId);

        const eGovId: IEGovId = eGovIdDApp.id as IEGovId;

        expect(eGovId.hasErrors()).toBeUndefined();
        expect(eGovId["__ssi"]).toBeDefined();

    }

    it("Creates Egovernance Wallets", (callback) => {

        const eGovDApp = getEGov()

        const keyGenArgs = getCreds(eGovDApp);

        const sc: DSUSecurityContext = getSCApi().refreshSecurityContext();

        sc.on(OpenDSUInitializationEvent, () => {
            const id = "ID" + Math.floor(Math.random() * 1000000);
            getW3cDIDApi().createIdentity(DSUDIDMethods.NAME, "default", id, (err: Err, did?: DSUDid) => {
                expect(err).toBeUndefined();
                expect(did).toBeDefined();

                const repo = new BlueprintOpenDSURepository(EGovDApp, toolkitInjectables, "default", did);

                repo.create(eGovDApp, keyGenArgs, (err: Err, newModel?: IEGovDApp, dsu?: DSU, ssi?: KeySSI) => {
                    if (err)
                        return callback(err);


                    expect(err).toBeUndefined();
                    expect(newModel).toBeDefined();
                    expect(dsu).toBeDefined();
                    expect(ssi).toBeDefined()

                    console.log(ssi?.getIdentifier())
                    validateEGovModel(newModel as IEGovDApp);

                    validateEGovDAppDSU(dsu as DSU, (err) => {
                        expect(err).toBeUndefined();
                        validateIDDSU((newModel?.id?.id as { [indexer: string]: any })["__ssi"] as string, (err: Err) => {
                            expect(err).toBeUndefined();
                            callback();
                        })
                    })
                })
            })
        })
    })
})