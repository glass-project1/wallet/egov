export default {
  "appName": "egov-dapp",
  "version": "%{VERSION}",
  "vault": "%{VAULT}",
  "agent": "browser",
  "system":   "any",
  "browser":  "any",
  "mode":  "dev-secure",
  "domain": "%{DOMAIN}",
  "stage": "%{NODE_ENV}",
  "didDomain":  "%{DID_DOMAIN}",
  "enclaveType": "WalletDBEnclave",
  "vaultDomain": "%{VAULT_DOMAIN}",
  "sw": true,
  "pwa": false,
  "basePath": "/egov-dapp/loader/",
  "allowPinLogin": true
}
