#!/bin/bash -xe
# As e2e tests seem to have trouble with TypeORM
# A simple CURL script to create an EGovDApp in concurrency

function postCurl () {
    curl -X 'POST' \
        "http://localhost:3000/borest/egov/egov/$1" \
        -H 'accept: application/json' \
        -d ''
}

CURL_PATH=$(which curl)
if [ ! -x "${CURL_PATH}" ]
then
    echo 1>&2 "curl must be on PATH. Can be installed with sudo apt install curl"
    exit 1
fi;

PARALLEL_PATH=$(which env_parallel)
if [ ! -x "${PARALLEL_PATH}" ]
then
    echo 1>&2 "GNU parallel must be on PATH. Can be installed with sudo apt install parallel"
    exit 1
fi;
source `which env_parallel.bash`

# env_parallel postCurl ::: 10000022815 10000022818 10000022819 10000022820 10000022821 10000022822 10000022823 10000022824 10000022825 10000022826
env_parallel postCurl ::: 10000022813 10000022815 10000022816 10000022818 10000022819 10000022820 10000022821 10000022822 10000022823 10000022824 10000022825 10000022826 10000022827 10000022828 10000022829 10000022830 10000022831 10000022832 10000022833