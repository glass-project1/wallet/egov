#!/bin/bash -xe
# As e2e tests seem to have trouble with TypeORM
# A simple CURL script to create an EGovDApp
CURL_PATH=$(which curl)
if [ ! -x "${CURL_PATH}" ]
then
    echo 1>&2 "curl must be on PATH"
    exit 1
fi;
# 10000022815 is TK and has a passport
# 10000022818 is TK and does not have a passport
curl -X 'POST' \
  'http://localhost:3000/borest/egov/egov/10000022815' \
  -H 'accept: application/json' \
  -d ''