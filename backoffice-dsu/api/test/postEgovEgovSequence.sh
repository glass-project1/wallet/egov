#!/bin/bash -xe
# As e2e tests seem to have trouble with TypeORM
# A simple CURL script to create an EGovDApp

function postCurl () {
    curl -X 'POST' \
        "http://localhost:3000/borest/egov/egov/$1" \
        -H 'accept: application/json' \
        -d ''
}

CURL_PATH=$(which curl)
if [ ! -x "${CURL_PATH}" ]
then
    echo 1>&2 "curl must be on PATH"
    exit 1
fi;
# 10000022815 is TK and has a passport
# 10000022818 is TK and does not have a passport
for ID in 10000022815 10000022818 10000022819 10000022820 10000022821
#for ID in 10000022821 10000022823 10000022824 10000022825 10000022826
#for ID in 10000022827 10000022828 10000022829 10000022830 10000022831
do
    postCurl "${ID}"
done
