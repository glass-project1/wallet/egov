import {
    Err,
    Callback,
    errorCallback,
    debug,
    LoggedError,
    info,
    error,
    CriticalError,
    getLogger, LOGGER_LEVELS
} from '@glass-project1/logging';
import {KeySSI, OpenDSU, getOpenDSU, getHttpApi, getKeySSIApi, GenericCallback} from "@glass-project1/opendsu-types";
import {
    ServiceEnvironmentDefinition,
    registerGlassDIDMethod,
    EGovServiceWallet, GlassDIDSchema, HTTP_METHODS, MessagingHub, getToolkitInjectables
} from '@glass-project1/glass-toolkit';
import { bootServiceWallet } from "@glass-project1/glass-toolkit/lib/providers/utility/initializations";
import {
    BackendFileServiceImp,
    generateServiceEnvironment,
    getEnvironmentFromProcess,
} from "@glass-project1/glass-toolkit/lib/providers";
import {
    WorkerManager, WorkerManagerConfig
} from "@glass-project1/glass-toolkit/lib/providers/concurrency";

import { getSeedStorage } from "@glass-project1/glass-toolkit/lib/providers/persistence";
import { EnclaveSeedStorage } from "@glass-project1/dsu-blueprint/lib/cli/persistence";
import {OpenDSURepository} from "@glass-project1/dsu-blueprint";
import {getInjectablesRegistry, injectable} from "@glass-project1/db-decorators";

type EndpointDef = {
    "baseEndpoint": string,
    "baseEndpointSuffix": string,
    domain?: string
}

export const COUNTRY_CODE_KEY = "GLASS_COUNTRY_CODE";
export const ENVIRONMENT_SUFFIX = "ENV_SUFFIX";

@injectable("ServiceWalletBooter")
export class ServiceWalletBooter {
  
    private walletCache: EGovServiceWallet;
    private storage: EnclaveSeedStorage;
    private fileService: BackendFileServiceImp;

    readonly countryCode: string;

    readonly environment: ServiceEnvironmentDefinition;

    private workerManager: WorkerManager

    constructor() {
   
        this.countryCode = (process.env[COUNTRY_CODE_KEY] || "pt").toLowerCase();
        this.environment = generateServiceEnvironment(getEnvironmentFromProcess());

        console.log(process.env);
        
        getToolkitInjectables(); // set toolkit injectables

        getLogger().setLevel(LOGGER_LEVELS.DEBUG)
        const self = this;

        const exitHandler = async (code: number | Error) => {
            return new Promise<void>((resolve) => {
                info.call(self, "Wrapping up service before exiting with {1} {0}...", code, typeof code === "number" ? "code" : "Error");
                if (!self.workerManager)
                    return resolve()

                self.workerManager.shutdown().then(() => {
                    info.call(self, "Waiting for workers to shutdown...");
                    setTimeout(() => {
                        resolve()
                    }, 1000)
                }).catch(e => {
                    error.call(self, "Failed to signal shutdown to Workers: {0}", e)
                    resolve()
                })
            })
        }

        process
            .on("beforeExit", exitHandler)
            .on("uncaughtException", (err) => {
                exitHandler(err)
                    .then(() => process.exit(1))
            })
            .on("unhandledRejection", (reason) => {
                const err = new CriticalError("Unhandled Rejection: {0}", self, reason);
                exitHandler(err)
                    .then(() => process.exit(1))
            })
            .on("exit", (code) => {
                info.call(self, "Exiting with code {0} without cleanup...", code);
            })


        info.call(this,`Service wallet boot with country code: ${this.countryCode}`);
    }

    private async getEndpoints(endpoint = "glass.json"): Promise<EndpointDef>{
        // eslint-disable-next-line @typescript-eslint/no-var-requires
        const fs = require('fs');
        // eslint-disable-next-line @typescript-eslint/no-var-requires
        const path = require('path');

        let endpoints: EndpointDef;
        try {
            endpoints = JSON.parse(fs.readFileSync(path.join(process.cwd(), "./assets/endpoints", endpoint))) as EndpointDef
        } catch (e: any) {
            throw new Error(`Failed to read ${endpoint} over: ${e.message || e}`);
        }
        return endpoints;
    }

    private getServiceInfo(environment: ServiceEnvironmentDefinition, callback: GenericCallback<KeySSI>){
        const self: ServiceWalletBooter = this;

        function getHeaders(httpMethod: HTTP_METHODS, body?: Record<string, unknown>){
            if (!body && httpMethod !== HTTP_METHODS.GET)
                throw new LoggedError("No body provided to request")

            const headers: {[indexer: string]: any} = {};
            headers['Content-Type'] = 'application/json';

            if (httpMethod !== HTTP_METHODS.GET)
                headers['Content-Length'] = `${Buffer.byteLength(JSON.stringify(body))}`;
            return headers;
        }

        self.getEndpoints("glass.json")
            .then((endpoint: EndpointDef) => {
                console.log(JSON.stringify(endpoint))
                self.fileService.getSchema((err: Err, schema?: Record<string, unknown>) => {
                    if (err || !schema)
                        return errorCallback(err || "missing schema", callback);
                    const didSchema = new GlassDIDSchema(schema);
                    const errs = didSchema.hasErrors("createdOn", "createdBy", "updatedOn", "updatedBy");
                    if (errs)
                        return errorCallback("Schema from file is invalid: {0}", callback, errs.toString());

                    debug.call(self, "Requesting DID schema: {0}", didSchema.toString())
                    const data = {
                        didSchema: schema,
                        // domain: environment.vaultDomain // está assim por conta do trafik
                        domain: `glass${!!process.env[ENVIRONMENT_SUFFIX] ? process.env[ENVIRONMENT_SUFFIX] : ""}`
                    }
                    const httpMethod = HTTP_METHODS.POST;
                    const headers = getHeaders(httpMethod, data)
                    const reqOptions = {
                        headers: headers,
                        method: HTTP_METHODS.POST,
                        mode: "cors",
                        body: JSON.stringify(data)
                    }

                    const url = `${endpoint.baseEndpoint}${endpoint.baseEndpointSuffix}/did/create`
                    console.log(url)
                    debug.call(self, "Calling Glass Trust Manager on {0} to create new service wallet with options {1}.", url, JSON.stringify(reqOptions, undefined, 2))
                    getHttpApi().fetch(url, reqOptions)
                        .then(async (response: any) => {
                            let ssi: KeySSI;
                            try {
                                response = JSON.parse(await response.text());
                                debug.call(self, "received {0}", JSON.stringify(response));
                                ssi = getKeySSIApi().parse(response);
                            } catch (e: any){
                                return errorCallback.call(self, e, callback);
                            }
                            callback(undefined, ssi);
                        })
                        .catch((e: any) => errorCallback.call(self, e, callback))
                })
            })
            .catch((e: any) => errorCallback.call(self, e, callback));
    }

    /**
     * 
     * @returns Promise
     */
    async load(): Promise<any> {
        const self: ServiceWalletBooter = this;
        return new Promise<EGovServiceWallet>((resolve, reject) => {

            const cb = (err: Err, wallet?: EGovServiceWallet) => {
                if (err || !wallet)
                    return errorCallback.call(self, err, reject)
                resolve(wallet)
            }

            try {
                self.getOpenDSU();
            } catch (e: any) {
                return cb(e)
            }

            // Override the domain and vaultDomain from the process environment.
            // Should not be needed, if the process has the right config,
            // as env variables DOMAIN, DID_DOMAIN e VAULT_DOMAIN

            self.fileService = new BackendFileServiceImp(`./assets/modg/${self.countryCode}`)

            if (self.walletCache)
                return cb(undefined, self.walletCache);

            self.initializeStorage(self.environment, (err) => {
                if(err)
                    return cb(err);

                self.storage.getCurrentKeySSI((err: Err, ssi?: KeySSI) => {
                    const promise = err || !ssi ? self.getNewServiceWallet.bind(self) : self.loadServiceWallet.bind(self)
                    promise(ssi || self.environment)
                        .then(resolve)
                        .catch(reject)
                })
            })
        })
    }

    private async getNewServiceWallet(environment: ServiceEnvironmentDefinition){
        const self: ServiceWalletBooter = this;
        return new Promise<EGovServiceWallet>((resolve, reject) => {
           self.getServiceInfo(environment, (err: Err, walletSSI?: KeySSI) => {
               if (err || !walletSSI)
                   return errorCallback.call(self, err || "No KeySSI for the service wallet received", reject);
               self.storage.storeKeySSI(walletSSI.getIdentifier(), (err: Err) => {
                   if (err)
                       return errorCallback.call(self, err || "Failed to store the wallet SSI", reject);
                   self.loadServiceWallet(walletSSI)
                       .then(resolve)
                       .catch(reject)
               })
           })
        })
    }

    private async loadServiceWallet(ssi: string | KeySSI){
        const self: ServiceWalletBooter = this;
        return new Promise<EGovServiceWallet>((resolve, reject) => {
            const repo = new OpenDSURepository(EGovServiceWallet);
            repo.read(ssi, (err: Err, wallet?: EGovServiceWallet) => {
                if (err || !wallet)
                    return errorCallback.call(self, err || "Could not resolve service wallet", reject);
                bootServiceWallet(wallet, false, (err: Err) => {
                    if (err)
                        return errorCallback.call(self, err, reject);
                    self.walletCache = wallet;

                    const messageHub = getInjectablesRegistry().get("MessageHub") as MessagingHub;

                    if (!messageHub)
                        return errorCallback.call(self, "Failed to retrieve message hub", reject);

                    const cfg: WorkerManagerConfig = {
                        ssi: typeof ssi === 'string' ? ssi : ssi.getIdentifier(),
                        workerLimit: 10,
                        countryCode: this.countryCode,
                        openDSUPath: "../privatesky/psknode/bundles/openDSU",
                        taskPath: [
                            "../node_modules/@glass-project1/glass-toolkit/lib/providers/concurrency/tasks",
                            "./dist/tasks"
                        ],
                        fileName: "../node_modules/@glass-project1/glass-toolkit/lib/providers/concurrency/worker.js",
                        idleTime: 60,
                        logging: LOGGER_LEVELS.DEBUG
                    }

                    self.workerManager = new WorkerManager(cfg)

                    messageHub.startListening();
                    resolve(wallet);
                })
            })
        })
    }

    /**
     * Utility method to require - load in runtime - OpenDSU.
     * Must be the last thing in "required" in runtime,
     * because it will break the "require" implementation.
     * @returns
     */
    private getOpenDSU() : OpenDSU {
      // eslint-disable-next-line @typescript-eslint/no-var-requires
        const path = require("path");

        const opendsuPath = path.join(process.cwd(), '../privatesky/psknode/bundles/openDSU')
      // eslint-disable-next-line @typescript-eslint/no-var-requires
      const opendsuUntyped = require(opendsuPath); // delayed here due to https://gitlab.com/glass-project1/wallet/egov-id/-/issues/1#note_27486
      //console.log("OpenDSU required", opendsuUntyped);
      //warn("Accessing OpenDSU");
      const opendsu = getOpenDSU();
      registerGlassDIDMethod();
      return opendsu;
    }

    /**
     * Create new enclave storage
     * 
     * @param  {any} environment
     * @param  {Callback} callback
     * @returns void
     */
    private initializeStorage(environment: any, callback: Callback): void{
      const self = this;

      const storageEnv = Object.assign({}, environment, {
        name: !!environment.name ? environment.name  : `egov.${this.countryCode}`,
        vaultDomain: "vault"
      });

      if(!self.storage)
        self.storage = getSeedStorage(storageEnv) as EnclaveSeedStorage;
      
      self.storage.initialize((err: Err) => {
        if(err)
          return callback(err);

          callback();
      })
    }
  
}
  