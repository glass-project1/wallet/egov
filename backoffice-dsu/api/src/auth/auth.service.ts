import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { EGovUser } from '../egov/egovuser.entity';
import { EGovUserService } from '../egov/egovuser.service';

@Injectable()
export class AuthService {
    constructor(
        private egovUserService: EGovUserService,
        private jwtService: JwtService
    ) { }

    /**
     * Validate a username/password.
     * @param egovUsername
     * @param egovPassHash password in clear text.
     * @returns an EGovUser if matched. null if not matched.
     */
    async validateUser(egovUsername: string, egovPassHash: string): Promise<EGovUser> {
        console.log("AuthService.validateUser ", egovUsername, egovPassHash);
        if (!egovUsername) {
            console.log("AuthService.validateUser returned null because of missing username");
            return null;
        }
        const egovUserCollection = await this.egovUserService.findByEmail(egovUsername);
        console.log("AuthService.validateUser found ", egovUserCollection);
        if (!egovUserCollection || egovUserCollection.length == 0) {
            console.log("AuthService.validateUser returned null because username not found!");
            return null;
        }
        if (egovUserCollection[0].passHash === egovPassHash // TODO clear text comparison to bcrypt
        ) {
            console.log("AuthService.validateUser returned ", egovUserCollection[0]);
            return egovUserCollection[0];
        }
        console.log("AuthService.validateUser returned null");
        return null;
    }

    /**
     * Transforms a valid EGovUser into a valid (signed) JWT token.
     * @param egovUser an EGovUser object, as returned by LocalStrategy.validate()
     * @returns an object with the JWT authentication token. Please document the return type in the auth.controller.ts login method
     */
    async login(egovUser: EGovUser) {
        const payload = {userId: egovUser.userId, email: egovUser.email};
        return {
            userId: egovUser.userId,
            email: egovUser.email,
            token: this.jwtService.sign(payload),
        };
    }

    /**
     * Marks this JWT token as expired.
     * @param au an EGovUser object, as returned by JwtStrategy.validate()
     * @returns an object with the JWT authentication token.
     */
    async logout(au: any) {
        // TODO
        return au;
    }
}
