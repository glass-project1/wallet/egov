import { Module } from '@nestjs/common';
import { EGovModule } from './egov/egov.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
@Module({
  imports: [EGovModule, AuthModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
