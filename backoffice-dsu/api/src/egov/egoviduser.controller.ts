import {Connection} from "typeorm";
import {Body, Controller, Delete, Get, Param, Post, Put, Query, UseGuards} from "@nestjs/common";
import {ApiBearerAuth, ApiOperation, ApiTags} from "@nestjs/swagger";
import {EGovUser} from "./egovuser.entity";
import {AuthGuard} from "@nestjs/passport";

@ApiTags("EGovUser")
@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth()
@Controller("/egov/egovuser")
export class EGovUserController {
    constructor(private connection: Connection) {
    }

    @Get()
    @ApiOperation({summary: "Get all EGovUsers"})
    async findAll(@Query() query: EGovUser): Promise<EGovUser[]> {
        let egovUserCollection = await EGovUser.find({order: {userId: "ASC"}});
        console.log("egovidUser.findAll, egovUserColletion =", egovUserCollection);
        return egovUserCollection;
    }

    @Put(":id")
    @ApiOperation({summary: "Update one EGovUser"})
    async update(@Param("id") id: string, @Body() egovUser: EGovUser): Promise<EGovUser> {
        console.log("egovUser.update... egovUser=", egovUser);
        const egovUserRepository = this.connection.getRepository(EGovUser);
        await egovUserRepository.save(egovUser);
        console.log("egovUser.update, egovUser =", egovUser);
        return egovUser;
    }

    @Post()
    @ApiOperation({summary: "Create one EGovUser"})
    async create(@Body() egovUser: EGovUser): Promise<EGovUser> {
        console.log("egovidUser.create... egovUser=", egovUser);
        const egovUserRepository = this.connection.getRepository(EGovUser);
        await egovUserRepository.save(EGovUser);
        console.log("egovidUser.create, egovidUser =", egovUser);
        return egovUser;
    }

    @Get(":id")
    @ApiOperation({summary: "Get one EGovIdUser"})
    async findOne(@Param("id") id: string): Promise<EGovUser> {
        console.log("egovUser.findOne... id=", id);
        let egovUser = await EGovUser.findOne(id);
        console.log("egovUser.findOne egovUser =", egovUser);
        return egovUser;
    }

    @Delete(":id")
    @ApiOperation({summary: "Delete one EGovUser"})
    async remove(@Param("id") id: string): Promise<void> {
        console.log("egovUser.delete... id=", id);
        const egovUserRepository = this.connection.getRepository(EGovUser);
        const delResult = await egovUserRepository.delete(id);
        console.log("egovUser.delete, egovUser =", delResult.raw);
        return;
    }
}
