import { BaseEntity, Column, Entity, PrimaryColumn } from "typeorm";
import { ApiProperty } from '@nestjs/swagger';



@Entity("egov")
export class EGovEntity extends BaseEntity {

    @ApiProperty({ description: "eGovId ID - in the PT reality, it could be one of eGovIdId, eGovTaxId, eGovSSN, but for initial simplification it is assumed to be the eGovIdId.", required: false })
    @PrimaryColumn({ name: "egovidid" })
    eGovIdId: string;

    @ApiProperty({ description: "DID identifier of the EGovDApp" })
    @Column({ name: "did" })
    did: string;

    @ApiProperty({ description: "A JSON containing the EGovDApp DSU seed keySSI. Non-null if the wallet has already been created. See AppResource.key=egovid.dapp.keyTemplate for an example of such JSON.", required: false })
    @Column({ name: "walletkeyssi" })
    walletKeySSI: string;

    @ApiProperty({ description: "A timestamp when the record was created. Needed to sort results by time of creation.", required: false })
    @Column({ name: "createdon", type: 'timestamptz' })
    createdOn: Date; 
}
