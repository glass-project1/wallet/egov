import {inject} from "@glass-project1/db-decorators";
import {BlueprintOpenDSURepository} from '@glass-project1/dsu-blueprint';
import {
    EGovInstallRequest,
    EGovServiceWallet,
    getToolkitInjectables,
    GlassDID,
    GlassDIDSchema,
    GlassWalletManager,
    IEGovIdDApp,
    IEGovPassportDApp,
    MarketKeys
} from '@glass-project1/glass-toolkit';
import {EGovDApp} from '@glass-project1/glass-toolkit/lib/blueprints/eGovernance/EGovDApp';
import {EGovPassport} from "@glass-project1/glass-toolkit/lib/blueprints/eGovernance/EGovPassport";
import {EGovPassportDApp} from "@glass-project1/glass-toolkit/lib/blueprints/eGovernance/EGovPassportDApp";
import {
    generateWalletEnvironment,
    getEnvironmentFromProcess,
    getServiceWallet
} from "@glass-project1/glass-toolkit/lib/providers";
import {WorkerManager, WorkerResponse} from "@glass-project1/glass-toolkit/lib/providers/concurrency";
import {debug, Err, error, errorCallback, info, LoggedError} from '@glass-project1/logging';
import {DSU, DSUDid, KeySSI} from "@glass-project1/opendsu-types";
import {HttpService} from '@nestjs/axios';
import {Injectable} from '@nestjs/common';
import {PaginatedDto} from 'src/paginated.dto';
import {Connection, EntityManager} from 'typeorm';
import {ENVIRONMENT_SUFFIX, ServiceWalletBooter} from "../ServiceWalletBooter";
import {AppResourceRepository} from './appresource.repository';
import {EGovEntity} from './egov.entity';
import {EGovRepository} from './egov.repository';
import {EGovBirthCertificateCreateResponse} from './egovbirthcertificatecreateresponse';
import {EGovCriminalRecordCreateResponse} from './egovcriminalrecordcreateresponse';
import {EGovEducationCreateResponse} from './egoveducationcreateresponse';
import {EGovHealthCreateResponse} from './egovhealthcreateresponse';
import {EGovIdCreateResponse} from './egovidcreateresponse';
import {EGovPassportCreateResponse} from './egovpassportcreateresponse';
import {EGovTaxCreateResponse} from './egovtaxcreateresponse';
import {
    ServiceWalletCreationModel, ServiceWalletCreationResult
} from "@glass-project1/glass-toolkit/lib/providers/concurrency/tasks/ServiceWalletCreationTask";
import {EGovDAppCreationModel, EGovDAppCreationResult} from "../tasks/EGovDAppCreationTask";
import { EGovIdLockedResponse } from "./egovidlockedresponse";

/*
   Imports from outside the api folder will force tsconfig to re-create the dist folder structure.
   These where used for the model classes, but once the model moved into glass-toolkit,
   it is no longer needed!
   privatesky is imported using require.
*/
// import { Dummy } from "../../../src/model"; // this works, but forces dist/api/src
// import { EGovId } from "./model";
/* the above line does not work, even if adding to tsconfig.json

    "compilerOptions": {
      "rootDirs": ["./src", "../src"] // or "../../src"
    }
*/
//import {safeParseKeySSI} from "@glass-project1/dsu-blueprint/lib";
//import opendsu = require('../../../privatesky/psknode/bundles/openDSU'); // if here, will break https://gitlab.com/glass-project1/wallet/egov-id/-/issues/1#note_27486


// just a triplet needed below...
type AuxEGovServiceEGovServiceWalletDsuSsi = {
    serviceWallet: EGovServiceWallet;
    dsu: DSU;
    keySSI: KeySSI;
};

@Injectable()
export class EGovService {

    private eGovRepository: EGovRepository;
    arcRepository: AppResourceRepository;

    @inject("GlassWalletManager")
    walletManager!: GlassWalletManager;

    @inject("ServiceWalletBooter")
    walletBooter!: ServiceWalletBooter;

    @inject("WorkerManager")
    workerManager!: WorkerManager;

    constructor(
        private connection: Connection,
        private readonly httpService: HttpService,
    ) {
        this.arcRepository = this.connection.getCustomRepository(AppResourceRepository);
        this.eGovRepository = this.connection.getCustomRepository(EGovRepository);
    }
    
    /**
     * Create (INSERT) a new EGovID from DTO JSON data in a single transaction.
     * @param eGovDto data to be inserted, from JSON. Will be mutated by adding PKs and internal FKs.
     */
    async create(eGovDto: any) {
        const self = this;
        await this.connection.transaction(async tem => {
            await self.createT(tem, eGovDto);
        });
    }

    /**
     * Create (INSERT) a new ClinicalTrial from DTO JSON data, given a transactional entity manager.
     * @param tem Transactional EntityManager
     * @param eGovDto data to be inserted, from JSON. Will be mutated by adding PKs and internal FKs.
     */
    async createT(tem: EntityManager, eGovDto: any) {
        await tem.save(EGovEntity, eGovDto);
    }

    /**
     * Update (SQL UPDATE) a EGov from DTO JSON data in a single transaction.
     * @param eGovDto data to be inserted, from JSON. Will be mutated by adding PKs and internal FKs.
     */
    async update(eGovDto: any) {
        const self = this;
        await this.connection.transaction(async tem => {
            await self.updateT(tem, eGovDto);
        });
    }

    /**
     * Update (SQL UPDATE) a EGov from DTO JSON data in a single transaction.
     * @param tem Transactional EntityManager
     * @param eGovDto data to be inserted, from JSON. Will be mutated by adding PKs and internal FKs.
     */
    async updateT(tem: EntityManager, eGovDto: any) {
        await tem.save(EGovEntity, eGovDto); // autocommit is good enough ?
    }

    /**
     * Create a new DSU wallet for a given citizenship ID number.
     * If this ID already has a wallet, the same wallet is returned.
     * No more than one wallet can be created for the same ID.
     *
     * The algorithm is as follows:
     * step 1 - BEGIN TRANSACTION
     * step 2 - SELECT FOR UPDATE to lock the record being modified.
     * step 3 - If alredy has a wallet, return it and stop here.
     * step 4 - Create the DSU using an external DSU service with a callback
     * step 5 - UPDATE the record previously locked
     * step 6 - COMMIT
     *
     * Error handling must rollback and release the transaction.
     *
     * @param {string} eGovEGovIdId citizen ID number of the record to create the wallet.
     */
    async createWallet(eGovEGovIdId: string): Promise<EGovEntity|EGovIdLockedResponse> {
        const self = this;
        const debugId = "egov createWallet "+eGovEGovIdId;
        let eGovEntity: EGovEntity | undefined = undefined;
 
        let resultResolve;
        let resultReject;
        const result = new Promise<EGovEntity>((resolve, reject) => {
            resultResolve = resolve;
            resultReject = reject;
        });

        const queryRunner = this.connection.createQueryRunner();
        await queryRunner.startTransaction();
        result.catch(async (err) => {
            // from now on, if we reject the result, we need to rollback and close the DB query.
            // this.initService.releaseOpenDSU(openDsuLockedFlag, debugId);
            await queryRunner.rollbackTransaction();
            await queryRunner.release();
            // throw new Error(err); // do not (re) throw, or nestjs will die. Rejection is enough.
        });
        try {
            await queryRunner.manager.query("LOCK TABLE egov IN EXCLUSIVE MODE"); // egov-id #22 only one creation at a time - released on COMMIT/ROLLBACK
            console.log("Exclusive egov lock aquired for "+debugId);
            // This exclusive lock seems to drastic and makes the following pessimistic lock redundant,
            // but please do not remove the odsuWaitQueue, as concurrent requests may arrive to create «
            // serviceWallets.

            eGovEntity = await queryRunner.manager.findOne(EGovEntity, undefined, { where: { eGovIdId: eGovEGovIdId }, lock: { mode: "pessimistic_write" } });
            if (eGovEntity) {
                // self.initService.releaseOpenDSU(openDsuLockedFlag, debugId);
                await queryRunner.rollbackTransaction();
                await queryRunner.release();
                resultResolve(eGovEntity);
                return result; // createWallet must return the promise
            }

            // don't lock OpenDSU right away, as we may still get some concurrency from other service creation
            const egovIdData = await self.egovIdGetEgovIdData(eGovEGovIdId) as EGovIdLockedResponse;

            if(egovIdData.locked) {
              await queryRunner.rollbackTransaction();
              await queryRunner.release();
              resultResolve(egovIdData);  
              return result; // createWallet must return the promise
            }


            const concurrencyCreationFlag = true;
            let eGovIdDAppCreateResponse : EGovIdCreateResponse;
            let eGovPassportDAppCreateResponse : EGovPassportCreateResponse | undefined;
            let eGovCriminalRecordDAppCreateResponse : EGovCriminalRecordCreateResponse | undefined;
            let eGovBirthCertificateDAppCreateResponse : EGovBirthCertificateCreateResponse | undefined;
            let eGovTaxDAppCreateResponse : EGovTaxCreateResponse | undefined;
            let eGovHealthDAppCreateResponse : EGovHealthCreateResponse | undefined;
            let eGovEducationDAppCreateResponse : EGovEducationCreateResponse | undefined;

            if (!concurrencyCreationFlag) {
                // non-concurrent version
                eGovIdDAppCreateResponse = await self.egovIdCreateEGovIdDApp(eGovEGovIdId);
                eGovPassportDAppCreateResponse = await self.egovPassportCreateEGovPassportDApp(eGovEGovIdId); // can return undefined, or reject on failure
                eGovCriminalRecordDAppCreateResponse = await self.egovCriminalRecordCreateEGovCriminalRecordDApp(eGovEGovIdId); // can return undefined, or reject on failure
                eGovBirthCertificateDAppCreateResponse = await self.egovBirthCertificateCreateEGovBirthCertificateDApp(eGovEGovIdId); // can return undefined, or reject on failure
                eGovTaxDAppCreateResponse = await self.egovTaxCreateEGovTaxDApp(eGovEGovIdId); // can return undefined, or reject on failure
                eGovHealthDAppCreateResponse = await self.egovHealthCreateEGovHealthDApp(eGovEGovIdId); // can return undefined, or reject on failure
                eGovEducationDAppCreateResponse = await self.egovEducationCreateEGovEducationDApp(eGovEGovIdId); // can return undefined, or reject on failure

            } else {
                // concurrent version
                const peGovIdDAppCreateResponse = self.egovIdCreateEGovIdDApp(eGovEGovIdId);
                const peGovPassportDAppCreateResponse = self.egovPassportCreateEGovPassportDApp(eGovEGovIdId); // can return undefined, or reject on failure
                const peGovCriminalRecordDAppCreateResponse = self.egovCriminalRecordCreateEGovCriminalRecordDApp(eGovEGovIdId); // can return undefined, or reject on failure
                const peGovBirthCertificateDAppCreateResponse = self.egovBirthCertificateCreateEGovBirthCertificateDApp(eGovEGovIdId); // can return undefined, or reject on failure
                const peGovTaxDAppCreateResponse = self.egovTaxCreateEGovTaxDApp(eGovEGovIdId); // can return undefined, or reject on failure
                const peGovHealthDAppCreateResponse = self.egovHealthCreateEGovHealthDApp(eGovEGovIdId); // can return undefined, or reject on failure
                const peGovEducationDAppCreateResponse = self.egovEducationCreateEGovEducationDApp(eGovEGovIdId); // can return undefined, or reject on failure

                await Promise.all([peGovIdDAppCreateResponse,peGovPassportDAppCreateResponse,peGovTaxDAppCreateResponse,peGovCriminalRecordDAppCreateResponse,peGovBirthCertificateDAppCreateResponse, peGovHealthDAppCreateResponse, peGovEducationDAppCreateResponse]); // need to use for try/catch    
                eGovIdDAppCreateResponse = await peGovIdDAppCreateResponse;
                eGovPassportDAppCreateResponse = await peGovPassportDAppCreateResponse;  
                eGovCriminalRecordDAppCreateResponse = await peGovCriminalRecordDAppCreateResponse;
                eGovBirthCertificateDAppCreateResponse = await peGovBirthCertificateDAppCreateResponse;
                eGovTaxDAppCreateResponse = await peGovTaxDAppCreateResponse;
                eGovHealthDAppCreateResponse = await peGovHealthDAppCreateResponse;
                eGovEducationDAppCreateResponse = await peGovEducationDAppCreateResponse;
            }

            // now extract mountseedssi
            const eGovIdDAppSeedSSI = self.egovIdExtractWalletSeedSSI(eGovIdDAppCreateResponse);
            console.log(`XXXXXX eGovIdDApp seed SSI ${eGovIdDAppSeedSSI}`);

            const eGovPassportDAppSeedSSI = self.egovPassportExtractWalletSeedSSI(eGovPassportDAppCreateResponse);
            console.log(`XXXXXX eGovPassportDApp seed SSI ${eGovPassportDAppSeedSSI}`);
 
            const eGovCriminalRecordDAppSeedSSI = self.egovCriminalRecordExtractWalletSeedSSI(eGovCriminalRecordDAppCreateResponse);
            console.log(`XXXXXX eGovCriminalRecordDApp seed SSI ${eGovCriminalRecordDAppSeedSSI}`);
 
            const eGovBirthCertificateDAppSeedSSI = self.egovBirthCertificateExtractWalletSeedSSI(eGovBirthCertificateDAppCreateResponse);
            console.log(`XXXXXX eGovBirthCertificateDApp seed SSI ${eGovBirthCertificateDAppSeedSSI}`);
 
            const eGovTaxDAppSeedSSI = self.egovTaxExtractWalletSeedSSI(eGovTaxDAppCreateResponse);
            console.log(`XXXXXX eGovTaxDApp seed SSI ${eGovTaxDAppSeedSSI}`);

            const eGovHealthDAppSeedSSI = self.egovHealthExtractWalletSeedSSI(eGovHealthDAppCreateResponse);
            console.log(`XXXXXX eGovHealthDApp seed SSI ${eGovHealthDAppSeedSSI}`);

            const eGovEducationDAppSeedSSI = self.egovEducationExtractWalletSeedSSI(eGovEducationDAppCreateResponse);
            console.log(`XXXXXX eGovEducationDApp seed SSI ${eGovEducationDAppSeedSSI}`);

            const env: {domain: string, didDomain: string, vaultDomain: string} = getEnvironmentFromProcess();

            // Resolve the ID's SSI to have access to its name nor the new government did
            const getDIDNameFromEGovID = async function(){
                return new Promise<GlassDIDSchema>((resolve, reject) => {
                    const idRepo = new BlueprintOpenDSURepository(undefined, getToolkitInjectables());
                    idRepo.read(eGovIdDAppSeedSSI, (err: Err, id?: IEGovIdDApp) => {
                        if (err || !id || !id.did || !(id.did as unknown as GlassDID)?.data || !((id.did as unknown as GlassDID)?.data as GlassDIDSchema).name)
                            return errorCallback.call(this, err || "missing did data from eGovID's did", reject);
                        resolve((id.did as unknown as GlassDID)?.data);
                    })
                })
            }

            const idDidSchema = await getDIDNameFromEGovID();

            const taskData: EGovDAppCreationModel = {
                id: eGovIdDAppSeedSSI,
                passport: eGovPassportDAppSeedSSI,
                criminal: eGovCriminalRecordDAppSeedSSI,
                birth: eGovBirthCertificateDAppSeedSSI,
                tax: eGovTaxDAppSeedSSI,
                health: eGovHealthDAppSeedSSI,
                education: eGovEducationDAppSeedSSI,
                environment: env,
                did: {
                    data: {
                        name: idDidSchema.name as string,
                        identifier: `individual.egov.${self.walletBooter.countryCode}`,
                        publishes: [
                            `individual.egov.${self.walletBooter.countryCode}.**.*`
                        ]
                    }
                },
                marketDomain: `glass${!!process.env[ENVIRONMENT_SUFFIX] ? process.env[ENVIRONMENT_SUFFIX] : ""}`
            }

            const res: EGovDAppCreationResult = await self.workerManager.run("EGovDAppCreationTask", taskData)
            const {walletSSI, walletDID} = res;
            eGovEntity = new EGovEntity();
            eGovEntity.did = walletDID
            eGovEntity.eGovIdId = eGovEGovIdId;
            eGovEntity.walletKeySSI = walletSSI
            eGovEntity.createdOn = new Date();
            await this.enrichKeySSI(eGovEntity);
            await queryRunner.manager.save(EGovEntity, eGovEntity);
            await queryRunner.commitTransaction();
            await queryRunner.release();

            // unlock egovid row
            await self.egovIdToggleLocked(eGovEGovIdId, false);

            resultResolve(eGovEntity);

        } catch (err) {
            //await queryRunner.rollbackTransaction();
            //await queryRunner.release();
            errorCallback.call(self, "Failed to create EGovDApp: {0}", resultReject, err)
        }

        // Don't add the finally. It will be called before the repo.create(...) calls the calback,
        // and we do not want to release the transaction
        //finally {
        //    await queryRunner.release();
        //}

        return result;
    }

    private async runServiceWalletCreationTask(domain: string, schema: GlassDIDSchema){
        const self = this;
        const data: ServiceWalletCreationModel = {
            domain: domain,
            didDomain: domain.includes("-vault") ? domain : `${domain}-vault`,
            vaultDomain: domain.includes("-vault") ? domain : `${domain}-vault`,
            schema: schema
        }

        info.call(self, "Staring Create Service Wallet task with model: {0}", JSON.stringify(data, undefined, 2))
        const result: ServiceWalletCreationResult = await self.workerManager.run("ServiceWalletCreationTask", data)
        return result.walletSSI;
    }


    /**
     * Internal method to create a createNewEGovServiceWallet.
     * Acquires and releases the odsuLock.
     * @param glassDidSchemaName
     * @param domain
     * @returns 
     */
    private async createNewEGovServiceWallet(glassDidSchema: Record<any, unknown>, domain: string): Promise<AuxEGovServiceEGovServiceWalletDsuSsi> {
        const self = this;
        const debugId = "createNewEGovServiceWallet "+JSON.stringify(glassDidSchema, undefined, 2)+" "+domain;

        let resultResolve;
        let resultReject;
        const result = new Promise<AuxEGovServiceEGovServiceWalletDsuSsi>((resolve, reject) => {
            resultResolve = resolve;
            resultReject = reject;
        });

        // #EgovId22 - one OpenDSU writing per domain at a time.
        // let openDsuLockedFlag = await self.initService.acquireOpenDSU(debugId);
        //console.log("rootWallet=", rootWallet);
        // TODO use domain

        getServiceWallet(undefined, this.walletManager.did!, new GlassDIDSchema(glassDidSchema), (err?: Err, serviceWallet?: EGovServiceWallet, dsu?: DSU, ssi?: KeySSI) => {
            // self.initService.releaseOpenDSU(openDsuLockedFlag, debugId);
            if (err || !serviceWallet || !dsu ||!ssi) {
                resultReject (err || "Missing serviceWallet data");
                return;
            }
            info(`Created a new EGovServiceWallet, seedSsi=${ssi.getIdentifier(true)}, environment={domain: ${serviceWallet.environment!.domain}, vaultDomain: ${serviceWallet.environment!.vaultDomain}, didDomain: ${serviceWallet.environment!.didDomain}`);
            resultResolve({ serviceWallet: serviceWallet, dsu: dsu, keySSI: ssi });
        });

        return result;
    }

    /**
     * Experimental: Other gov agencies (like EGovID, Passport, etc...) need their own
     * wallet. This wallet must be an EGovServiceWallet, signed by the root EGov DID.
     * This means that these ServiceWallets must be created in the process, and
     * only the seedSSI can then be returned to the owner.
     * WARNING: Every invocation creates a new one.
     * @param glassDidSchema
     * @param domain
     * @returns a (Promise to a) string with the seedSSI.
     */
     async createNewServiceWallet(glassDidSchema: Record<string, unknown>, domain: string): Promise<string> {

        info.call(this, "Delegating Service Wallet creation to Worker. Domain: {0}, schema {1}", domain, JSON.stringify(glassDidSchema, undefined, 2))

        return await this.runServiceWalletCreationTask(domain, new GlassDIDSchema(glassDidSchema));
    }

    decodeWalletKeySSI(eGovEntity : EGovEntity): string | undefined {
        if (!eGovEntity || !eGovEntity.walletKeySSI)
            return undefined;

        // if it is already JSON, do not enrich it
        let alreadyJson = false;
        let parsedJson : any = {};

        try {
            parsedJson = JSON.parse(eGovEntity.walletKeySSI);
            alreadyJson = true;
        } catch (e) {

        }
        if (alreadyJson)
            return parsedJson.payload.keySSI; // TODO not sure this is right
        else
            return eGovEntity.walletKeySSI;
    }

    /**
     * If eGovEntity.walletKeySSI is defined, enrich using the AppResource 
     * egov.dapp.keyTemplate as a JSON template.
     * 
     * @param eGovEntity 
     * @returns the same entity, with the property walletKeySSI enriched as a JSON string.
     */
    async enrichKeySSI(eGovEntity : EGovEntity): Promise<EGovEntity> {
        const self = this;
        if (!eGovEntity || !eGovEntity.walletKeySSI)
            return eGovEntity;

        // if it is already JSON, do not enrich it
        let alreadyJson = false;
        try {
            JSON.parse(eGovEntity.walletKeySSI);
            alreadyJson = true;
        } catch (e) {

        }
        if (alreadyJson)
            return eGovEntity;

        let requestPayload: string | { countryCode: string; name: string; description: string; publisher: string; version: string; icon: string; keySSI: string; } = await this.arcRepository.findConfigString(`egov.dapp.${self.walletBooter.countryCode}.keyTemplate`) as string;
        console.log(requestPayload);
        const re = new RegExp('_KEYSSI_TEMPLATE_','g');

        try {
            requestPayload = JSON.parse(requestPayload.replace(re, eGovEntity.walletKeySSI));
        } catch (e) {
            error.call(this, "Failed to Parse walletKeyTemplate")
            throw e;
        }

        const eGovInstallRequest: EGovInstallRequest = new EGovInstallRequest(requestPayload as { countryCode: string; name: string; description: string; publisher: string; version: string; icon: string; keySSI: string; });
        eGovEntity.walletKeySSI = JSON.stringify(eGovInstallRequest);
        return eGovEntity;
    }

    // Set of methods that are dedicated to call services on egov-id
    async egovIdGetEgovIdData(egovIdId : string) : Promise<EGovIdLockedResponse> {
      const self = this;
      const egovIdBorestUrl = await self.egovIdGetRestUrl();
      
      try {
          const getURL = `${egovIdBorestUrl}/egovid/egovid/${encodeURIComponent(egovIdId)}`;
          info("GET "+getURL);
          const egovIdAxiosReponse = await self.httpService.axiosRef.get(getURL);
          let eGovId: EGovIdLockedResponse = egovIdAxiosReponse.data;
          eGovId.locked = !!eGovId.locked ? true : false;
          
          // toglle row to locked
          if(!eGovId.locked) 
            await self.egovIdToggleLocked(eGovId.id, true);

          return eGovId;

      } catch (err) {
          if (err && err.response && err.response.data && err.response.data.message) {
              throw new Error(`POST ${egovIdBorestUrl} : ${err.response.data.statusCode} ${err.response.data.message}`);
          } else {
              throw new Error(`POST ${egovIdBorestUrl} : Error ${JSON.stringify(err)}`);
          }
      }
    }

    async egovIdToggleLocked(egovIdId: string, lock: boolean) : Promise<EGovIdLockedResponse> {
      const self = this;
      const egovIdBorestUrl = await self.egovIdGetRestUrl();
      try {
          const putURL = `${egovIdBorestUrl}/egovid/egovid/${encodeURIComponent(egovIdId)}/${lock ? 'lock' : 'unlock'}`;
          info("PUT "+putURL);
          const egovIdAxiosReponse = await self.httpService.axiosRef.put(putURL);
          let eGovId: EGovIdLockedResponse = egovIdAxiosReponse.data;

        return eGovId;
 
      } catch (err) {
          if (err && err.response && err.response.data && err.response.data.message) {
              throw new Error(`POST ${egovIdBorestUrl} : ${err.response.data.statusCode} ${err.response.data.message}`);
          } else {
              throw new Error(`POST ${egovIdBorestUrl} : Error ${JSON.stringify(err)}`);
          }
      }
    }
    
    /**
     * Invoke the egovid REST service to create an EGovIDDApp.
     * @param egovIdId 
     * @returns 
     * @throws (rejects) an error if the egovid REST service is unavailable.
     */
    async egovIdCreateEGovIdDApp(egovIdId : string) : Promise<EGovIdCreateResponse> {
        const self = this;
        const egovIdBorestUrl = await self.egovIdGetRestUrl();
        try {
            const postUrl = egovIdBorestUrl+"/egovid/egovid/"+encodeURIComponent(egovIdId);
            info("POST "+postUrl);
            const egovIdAxiosReponse = await self.httpService.axiosRef.post(postUrl);
            const eGovId : EGovIdCreateResponse = egovIdAxiosReponse.data;
            console.log(eGovId);
            return eGovId;
        } catch (err) {
            if (err && err.response && err.response.data && err.response.data.message) {
                throw new Error(`POST ${egovIdBorestUrl} : ${err.response.data.statusCode} ${err.response.data.message}`);
            } else {
                throw new Error(`POST ${egovIdBorestUrl} : Error ${JSON.stringify(err)}`);
            }
        }
    }

    egovIdExtractWalletSeedSSI(eGovIdCreateResponse : EGovIdCreateResponse): string | undefined {
        if (!eGovIdCreateResponse || !eGovIdCreateResponse.walletKeySSI) {
            return undefined;
        }
        try {
            const templateObj = JSON.parse(eGovIdCreateResponse.walletKeySSI);
            if (!templateObj || !templateObj.payload) {
                return undefined;
            }
            return templateObj.payload.keySSI;
        } catch (e) {
            return eGovIdCreateResponse.walletKeySSI;
        }
    }

    async egovIdListAll() : Promise<PaginatedDto<any, EGovIdCreateResponse>> {
        const self = this;
        const egovIdBorestUrl = await self.egovIdGetRestUrl();
        const egovIdListAxiosReponse = await self.httpService.axiosRef.get(egovIdBorestUrl+"/egovid/egovid");
        const eGovIdList : PaginatedDto<any, EGovIdCreateResponse> = egovIdListAxiosReponse.data;
        console.log(eGovIdList);
        return eGovIdList;
    }

    async egovIdGetRestUrl() : Promise<string> {
        const self = this;
        const egovIdBorestUrl = await self.arcRepository.findConfigString("egov.egovid.borestUrl");
        info("egov-id/borest URL is {0}", egovIdBorestUrl);
        return egovIdBorestUrl;
    }

    // Set of methods that are dedicated to call services on egov-passport

    /**
     * Invoke the egovpassport REST service to create an EGovPassportDApp.
     * @param egovPassportId 
     * @returns undefined if the egovpassport REST service returned 404.
     * @throws (rejects) an error if the egovpassport service is unavailable.
     */
    async egovPassportCreateEGovPassportDApp(egovPassportId : string) : Promise<EGovPassportCreateResponse | undefined> {
        const self = this;
        const egovPassportBorestUrl = await self.egovPassportGetRestUrl();
        try {
            const postUrl = egovPassportBorestUrl+"/egovpassport/egovpassport/"+encodeURIComponent(egovPassportId);
            info("POST "+postUrl);
            const egovPassportAxiosReponse = await self.httpService.axiosRef.post(postUrl);
            const eGovPassport : EGovPassportCreateResponse = egovPassportAxiosReponse.data;
            console.log(eGovPassport);
            return eGovPassport;
        } catch (err) {
            //console.log(err);
            if (err && err.response && err.response.data && err.response.data.message) {
                if (err.response.data?.statusCode == 404) {
                    console.log("RETURNED 404, passport is undefined");
                    return undefined;
                }
                throw new Error(`POST ${egovPassportBorestUrl} : ${err.response.data.statusCode} ${err.response.data.message}`);
            } else {
                throw new Error(`POST ${egovPassportBorestUrl} : Error ${JSON.stringify(err)}`);
            }
        }
    }

    egovPassportExtractWalletSeedSSI(eGovPassportCreateResponse : EGovPassportCreateResponse | undefined): string | undefined {
        if (!eGovPassportCreateResponse || !eGovPassportCreateResponse.walletKeySSI) {
            return undefined;
        }
        try {
            const templateObj = JSON.parse(eGovPassportCreateResponse.walletKeySSI);
            if (!templateObj || !templateObj.payload) {
                return undefined;
            }
            return templateObj.payload.keySSI;
        } catch (e) {
            return eGovPassportCreateResponse.walletKeySSI;
        }
    }

    async egovPassportGetRestUrl() : Promise<string> {
        const self = this;
        const egovPassportBorestUrl = await self.arcRepository.findConfigString("egov.egovpassport.borestUrl");
        info("egov-passport/borest URL is {0}", egovPassportBorestUrl);
        return egovPassportBorestUrl;
    }
   
    async egovPassportCreateDummy(egovPassportId) : Promise<EGovPassportCreateResponse> {
        const self = this;

        let resultResolve;
        let resultReject;
        const result = new Promise<EGovPassportCreateResponse>((resolve, reject) => {
            resultResolve = resolve;
            resultReject = reject;
        });

        const eGovPassportDummy = EGovPassport(getToolkitInjectables(), {
            //id: egovPassportId,
            passportNumber: "E" + egovPassportId,
            name: "name",
            givenName: "name",
            surname: "surname",
            issuingDate: new Date(),
            expirationDate: new Date(),
            issuingLocation: "issuingLocation",
            issuingAuthority: "issuingAuthority",
            birthDate: new Date(),
            birthLocation: "location birth",
            nationality: "nationality",
            gender: "gender",
            height: "height"
        });

        const env: {domain: string, didDomain: string, vaultDomain: string} = getEnvironmentFromProcess();

        const eGovPassportServiceWallet = await self.createNewEGovServiceWallet({name: "egovPassport"}, env.domain);

        const eGovPassportAgencyDID = eGovPassportServiceWallet.serviceWallet.did;

        const eGovPassportDApp = EGovPassportDApp(getToolkitInjectables(), {
            passport: eGovPassportDummy,
            did: {
                data: {
                    name: "egov-passport schema"
                }
            },
            environment: eGovPassportServiceWallet.serviceWallet.environment
        });

        console.log("Going to create eGovPassportDApp", eGovPassportDApp);

        const repoEGovPassportDApp = new BlueprintOpenDSURepository(EGovPassportDApp, getToolkitInjectables(), env.domain, eGovPassportAgencyDID as unknown as DSUDid);

        repoEGovPassportDApp.create(eGovPassportDApp, (err: Err, newEGovPassportDAppModel?: IEGovPassportDApp, dsu?: DSU, ssi?: KeySSI) => {
            if (err || !ssi) {
                resultReject(err || "Missing EGovPassportDApp ssi");
                return;
            }
            
            info("Created EGovPassportDApp with ssi="+ssi.getIdentifier(true));
            resultResolve(newEGovPassportDAppModel);
        });

        return result;
    }

 
    // Set of methods that are dedicated to call services on egov-criminalrecord (which is implemented by egov-id)

    /**
     * Invoke the egovcriminalrecord REST service to create an EGovCriminalRecordDApp.
     * @param egovPassportId just pass eGovIdId
     * @returns undefined if the egovcriminalrecord REST service returned 404.
     * @throws (rejects) an error if the egovcriminalrecord service is unavailable.
     */
     async egovCriminalRecordCreateEGovCriminalRecordDApp(egovCriminalRecordId : string) : Promise<EGovCriminalRecordCreateResponse | undefined> {
        const self = this;
        const egovCriminalRecordBorestUrl = await self.egovCriminalRecordGetRestUrl();
        try {
            const postUrl = egovCriminalRecordBorestUrl+"/egovid/egovcriminalrecord/"+encodeURIComponent(egovCriminalRecordId);
            info("POST "+postUrl);
            const egovPassportAxiosReponse = await self.httpService.axiosRef.post(postUrl);
            const eGovCriminalRecord : EGovCriminalRecordCreateResponse = egovPassportAxiosReponse.data;
            console.log(eGovCriminalRecord);
            return eGovCriminalRecord;
        } catch (err) {
            //console.log(err);
            if (err && err.response && err.response.data && err.response.data.message) {
                if (err.response.data?.statusCode == 404) {
                    console.log("RETURNED 404, passport is undefined");
                    return undefined;
                }
                throw new Error(`POST ${egovCriminalRecordBorestUrl} : ${err.response.data.statusCode} ${err.response.data.message}`);
            } else {
                throw new Error(`POST ${egovCriminalRecordBorestUrl} : Error ${JSON.stringify(err)}`);
            }
        }
    }

    egovCriminalRecordExtractWalletSeedSSI(eGovCriminalRecordCreateResponse : EGovCriminalRecordCreateResponse | undefined): string | undefined {
        if (!eGovCriminalRecordCreateResponse || !eGovCriminalRecordCreateResponse.walletKeySSI) {
            return undefined;
        }
        try {
            const templateObj = JSON.parse(eGovCriminalRecordCreateResponse.walletKeySSI);
            if (!templateObj || !templateObj.payload) {
                return undefined;
            }
            return templateObj.payload.keySSI;
        } catch (e) {
            return eGovCriminalRecordCreateResponse.walletKeySSI;
        }
    }

    async egovCriminalRecordGetRestUrl() : Promise<string> {
        const self = this;
        const egovCriminalRecordBorestUrl = await self.arcRepository.findConfigString("egov.egovcriminalrecord.borestUrl");
        info("egov-criminalrecord/borest URL is {0}", egovCriminalRecordBorestUrl);
        return egovCriminalRecordBorestUrl;
    }

    // Set of methods that are dedicated to call services on egov-birthcertificate (which is implemented by egov-id)

    /**
     * Invoke the egovbirthcertificate REST service to create an EGovCriminalRecordDApp.
     * @param egovPassportId just pass eGovIdId
     * @returns undefined if the egovcriminalrecord REST service returned 404.
     * @throws (rejects) an error if the egovcriminalrecord service is unavailable.
     */
    async egovBirthCertificateCreateEGovBirthCertificateDApp(egovBirthCertificateId : string) : Promise<EGovBirthCertificateCreateResponse | undefined> {
        const self = this;
        const egovBirthCertificateBorestUrl = await self.egovBirthCertificateGetRestUrl();
        try {
            const postUrl = egovBirthCertificateBorestUrl+"/egovid/egovbirthcertificate/"+encodeURIComponent(egovBirthCertificateId);
            info("POST "+postUrl);
            const egovBirthCertificateAxiosReponse = await self.httpService.axiosRef.post(postUrl);
            const eGovBirthCertificate : EGovBirthCertificateCreateResponse = egovBirthCertificateAxiosReponse.data;
            console.log(eGovBirthCertificate);
            return eGovBirthCertificate;
        } catch (err) {
            //console.log(err);
            if (err && err.response && err.response.data && err.response.data.message) {
                if (err.response.data?.statusCode == 404) {
                    console.log("RETURNED 404, passport is undefined");
                    return undefined;
                }
                throw new Error(`POST ${egovBirthCertificateBorestUrl} : ${err.response.data.statusCode} ${err.response.data.message}`);
            } else {
                throw new Error(`POST ${egovBirthCertificateBorestUrl} : Error ${JSON.stringify(err)}`);
            }
        }
    }

    egovBirthCertificateExtractWalletSeedSSI(eGovBirthCertificateCreateResponse : EGovBirthCertificateCreateResponse | undefined): string | undefined {
        if (!eGovBirthCertificateCreateResponse || !eGovBirthCertificateCreateResponse.walletKeySSI) {
            return undefined;
        }
        try {
            const templateObj = JSON.parse(eGovBirthCertificateCreateResponse.walletKeySSI);
            if (!templateObj || !templateObj.payload) {
                return undefined;
            }
            return templateObj.payload.keySSI;
        } catch (e) {
            return eGovBirthCertificateCreateResponse.walletKeySSI;
        }
    }

    async egovBirthCertificateGetRestUrl() : Promise<string> {
        const self = this;
        const egovBirthCertificateBorestUrl = await self.arcRepository.findConfigString("egov.egovbirthcertificate.borestUrl");
        info("egov-birthcertificate/borest URL is {0}", egovBirthCertificateBorestUrl);
        return egovBirthCertificateBorestUrl;
    }


    // Set of methods that are dedicated to call services on egov-tax

    /**
     * Invoke the egovtax REST service to create an EGovTaxDApp.
     * @param egovTaxId just pass eGovIdId
     * @returns undefined if the egovcriminalrecord REST service returned 404.
     * @throws (rejects) an error if the egovcriminalrecord service is unavailable.
     */
    async egovTaxCreateEGovTaxDApp(egovTaxId : string) : Promise<EGovTaxCreateResponse | undefined> {
        const self = this;
        const egovTaxBorestUrl = await self.egovTaxGetRestUrl();
        try {
            const postUrl = egovTaxBorestUrl+"/egovtax/egovtax/"+encodeURIComponent(egovTaxId);
            info("POST "+postUrl);
            const egovTaxAxiosReponse = await self.httpService.axiosRef.post(postUrl);
            const eGovTax : EGovTaxCreateResponse = egovTaxAxiosReponse.data;
            console.log(eGovTax);
            return eGovTax;
        } catch (err) {
            //console.log(err);
            if (err && err.response && err.response.data && err.response.data.message) {
                if (err.response.data?.statusCode == 404) {
                    console.log("RETURNED 404, tax is undefined");
                    return undefined;
                }
                throw new Error(`POST ${egovTaxBorestUrl} : ${err.response.data.statusCode} ${err.response.data.message}`);
            } else {
                // ECONNREFUSED vs HTTP 5xx in docker+proxy is complex. For now assume that whenever
                // egov-tax fails, there is no EGovTax record.
                // Change to throw new Error when all agree on turning on eGovTax.
                console.log("egov-tax error. Returning undefined.", err);
                return undefined;
                // throw new Error(`POST ${egovTaxBorestUrl} : Error ${JSON.stringify(err)}`);
            }
        }
    }

    egovTaxExtractWalletSeedSSI(eGovTaxCreateResponse : EGovTaxCreateResponse | undefined): string | undefined {
        if (!eGovTaxCreateResponse || !eGovTaxCreateResponse.walletKeySSI) {
            return undefined;
        }
        try {
            const templateObj = JSON.parse(eGovTaxCreateResponse.walletKeySSI);
            if (!templateObj || !templateObj.payload) {
                return undefined;
            }
            return templateObj.payload.keySSI;
        } catch (e) {
            return eGovTaxCreateResponse.walletKeySSI;
        }
    }

    async egovTaxGetRestUrl() : Promise<string> {
        const self = this;
        const egovTaxBorestUrl = await self.arcRepository.findConfigString("egov.egovtax.borestUrl");
        info("egov-tax/borest URL is {0}", egovTaxBorestUrl);
        return egovTaxBorestUrl;
    }

    /**
     * Invoke the egovHealth REST service to create an EGovHealthDApp.
     * 
     * @param egovHealthId just pass eGovIdId
     * @returns undefined if the egovhealth REST service returned 404.
     * @throws (rejects) an error if the egovhealth service is unavailable.
     */
    async egovHealthCreateEGovHealthDApp(egovHealthId : string) : Promise<EGovHealthCreateResponse | undefined> {
      const self = this;
      const egovHealthBorestUrl = await self.egovHealthGetRestUrl();
      try {
          const postUrl = egovHealthBorestUrl+"/egovhealth/egovhealth/"+encodeURIComponent(egovHealthId);
          info("POST "+postUrl);
          const egovHealthAxiosReponse = await self.httpService.axiosRef.post(postUrl);
          const eGovHealth : EGovHealthCreateResponse = egovHealthAxiosReponse.data;
          console.log(eGovHealth);
          return eGovHealth;
      } catch (err) {
          //console.log(err);
          if (err && err.response && err.response.data && err.response.data.message) {
              if (err.response.data?.statusCode == 404) {
                  console.log("RETURNED 404, Health is undefined");
                  return undefined;
              }
              throw new Error(`POST ${egovHealthBorestUrl} : ${err.response.data.statusCode} ${err.response.data.message}`);
          } else {
              // ECONNREFUSED vs HTTP 5xx in docker+proxy is complex. For now assume that whenever
              // egov-Health fails, there is no EGovHealth record.
              // Change to throw new Error when all agree on turning on eGovHealth.
              console.log("egov-Health error. Returning undefined.", err);
              return undefined;
              // throw new Error(`POST ${egovHealthBorestUrl} : Error ${JSON.stringify(err)}`);
          }
      }
    }

     /**
     * Invoke the egoveducation REST service to create an EGovEducationDApp.
     * 
     * @param egovEducationId just pass eGovIdId
     * @returns undefined if the egoveducation REST service returned 404.
     * @throws (rejects) an error if the egoveducation service is unavailable.
     */
     async egovEducationCreateEGovEducationDApp(egovEducationId : string) : Promise<EGovEducationCreateResponse | undefined> {
      const self = this;
      const egovEducationBorestUrl = await self.egovEducationGetRestUrl();
      try {
          const postUrl = egovEducationBorestUrl+"/egoveducation/egoveducation/"+encodeURIComponent(egovEducationId);
          info("POST "+postUrl);
          const egovEducationAxiosReponse = await self.httpService.axiosRef.post(postUrl);
          const eGovEducation : EGovEducationCreateResponse = egovEducationAxiosReponse.data;
          console.log(eGovEducation);
          return eGovEducation;
      } catch (err) {
          //console.log(err);
          if (err && err.response && err.response.data && err.response.data.message) {
              if (err.response.data?.statusCode == 404) {
                  console.log("RETURNED 404, Education is undefined");
                  return undefined;
              }
              throw new Error(`POST ${egovEducationBorestUrl} : ${err.response.data.statusCode} ${err.response.data.message}`);
          } else {
              // ECONNREFUSED vs HTTP 5xx in docker+proxy is complex. For now assume that whenever
              // egov-Education fails, there is no EGovEducation record.
              // Change to throw new Error when all agree on turning on eGovEducation.
              console.log("egov-Education error. Returning undefined.", err);
              return undefined;
              // throw new Error(`POST ${egovEducationBorestUrl} : Error ${JSON.stringify(err)}`);
          }
      }
    }

    egovHealthExtractWalletSeedSSI(eGovHealthCreateResponse : EGovHealthCreateResponse | undefined): string | undefined {
      if (!eGovHealthCreateResponse || !eGovHealthCreateResponse.walletKeySSI) {
          return undefined;
      }
      try {
          const templateObj = JSON.parse(eGovHealthCreateResponse.walletKeySSI);
          if (!templateObj || !templateObj.payload) {
              return undefined;
          }
          return templateObj.payload.keySSI;
      } catch (e) {
          return eGovHealthCreateResponse.walletKeySSI;
      }
    }

    async egovHealthGetRestUrl() : Promise<string> {
      const self = this;
      const egovHealthBorestUrl = await self.arcRepository.findConfigString("egov.egovhealth.borestUrl");
      info("egov-health/borest URL is {0}", egovHealthBorestUrl);
      return egovHealthBorestUrl;
    }


    egovEducationExtractWalletSeedSSI(eGovEducationCreateResponse : EGovEducationCreateResponse | undefined): string | undefined {
      if (!eGovEducationCreateResponse || !eGovEducationCreateResponse.walletKeySSI) {
          return undefined;
      }
      try {
          const templateObj = JSON.parse(eGovEducationCreateResponse.walletKeySSI);
          if (!templateObj || !templateObj.payload) {
              return undefined;
          }
          return templateObj.payload.keySSI;
      } catch (e) {
          return eGovEducationCreateResponse.walletKeySSI;
      }
    }

    async egovEducationGetRestUrl() : Promise<string> {
      const self = this;
      const egovEducationBorestUrl = await self.arcRepository.findConfigString("egov.egoveducation.borestUrl");
      info("egov-education/borest URL is {0}", egovEducationBorestUrl);
      return egovEducationBorestUrl;
    }
    

}

