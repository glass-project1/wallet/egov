
import { HttpException, HttpStatus } from '@nestjs/common';
import { PaginatedDto } from 'src/paginated.dto';
import { createQueryBuilder, EntityRepository, getManager, Repository } from 'typeorm';
import { EGovEntity } from './egov.entity';
import { EGovQuery } from './egovquery.validator';

@EntityRepository(EGovEntity)
export class EGovRepository extends Repository<EGovEntity>  {
    constructor(
    ) {
        super();
    }

    /**
     * Performs a SQL query applying the filters according to the @param
     * @param eGovSearchQuery
     */
     async search(eGovSearchQuery: EGovQuery): Promise<PaginatedDto<EGovQuery,EGovEntity>> {
        console.log('egov.repository.search query=', eGovSearchQuery)

        const escapeQuote = (str : string): string => {
            if (typeof str === 'string')
                return str.replace(/'/g, "''");
            else
                return str;
        };

        const transformValueToCommaList = (arr: string[] | string): string => {
            arr = Array.isArray(arr) ? arr : [arr]
            return arr.map(value => `'${escapeQuote(value)}'`).join(',');
        }

        const transformValueToEqualOrList = (fieldName: string, arr: string[] | string): string => {
            arr = Array.isArray(arr) ? arr : [arr]
            return "("+arr.map(value => `${fieldName} = '${escapeQuote(value)}'`).join(' OR ')+")";
        }

        const transformValueToLikeList = (fieldName: string, value: string[] | string): string => {
            const values = Array.isArray(value) ? value : [value];
            let str = '';
            let sep = '';
            values.forEach((value: string, index: number) => {
                str += sep;
                str += `${fieldName} ILIKE '%${escapeQuote(value)}%'`;
                sep = ' OR ';
            });
            return "( "+str+" )";
        }

        const getJsonWhereStatement = (fieldName: string, jsonProperty: string, values: string[] | string): string => {
            values = Array.isArray(values) ? values : [values]
            let str = ''
            values.forEach((value: string, index: number) => {
                if (index == 0) {
                    str += `${fieldName} ::jsonb @> \'{"${jsonProperty}":"${escapeQuote(value)}"}\'`
                } else {
                    str += `OR ${fieldName} ::jsonb @> \'{"${jsonProperty}":"${escapeQuote(value)}"}\'`
                }
            })
            return "( "+str+" )";
        }

        const getJsonWhereFieldLikeStatement = (fieldName: string, jsonProperty: string, values: string[] | string): string => {
            values = Array.isArray(values) ? values : [values];
            let str = '';
            let sep = '';
            values.forEach((value: string, index: number) => {
                str += sep;
                str += `${fieldName}::jsonb->>'${jsonProperty}' ILIKE '%${escapeQuote(value)}%'`;
                sep = ' OR ';
            });
            return "( "+str+" )";
        }

        /** NOTE: The name of "whereFunctions" need to be the same name of filter/properties of EventSearchQuery */
        const whereFunctions = {
            did(did: string[] | string): string {
                return `egov.did IN (${transformValueToCommaList(did)})`;
            },
            eGovIdId(eGovIdId: string[] | string): string {
                return `egov.eGovIdId IN (${transformValueToCommaList(eGovIdId)})`;
            },
            createdOnStart(date: string): string {
                return `egov.createdOn>=('${escapeQuote(date)}'::TIMESTAMPTZ)`; // check for injection issues ?
            },
            createdOnEnd(date: string): string {
                return `egov.createdOn<('${escapeQuote(date)}'::TIMESTAMPTZ)`; // check for injection issues?
            },
        }
        const sortProperties = {
            // prop names must match EGovIdQuerySortProperty
            "did":                 "egov.did",
            "eGovIdId":            "egov.eGovIdId",
            "createdOn":           "egov.createdOn",
        };

        let queryBuilder = await createQueryBuilder(EGovEntity, 'egov');
        //let whereSql : string = '';
        //let whereSqlSep : string = ' AND ';
        for (let [filterName, filterValue] of Object.entries(eGovSearchQuery)) {
            const whereFilter = whereFunctions[filterName]
            if (!!whereFilter) {
                const whereSqlClause = whereFilter(filterValue);
                queryBuilder.andWhere(whereSqlClause);
                //whereSql += `${whereSqlSep}${whereSqlClause}`;
            }
        }
        const orderByProps = Array.isArray(eGovSearchQuery.sortProperty) ? eGovSearchQuery.sortProperty : [eGovSearchQuery.sortProperty];
        const orderByDirs  = Array.isArray(eGovSearchQuery.sortDirection) ? eGovSearchQuery.sortDirection : [eGovSearchQuery.sortDirection];
        if (orderByProps.length != orderByDirs.length) {
            throw new HttpException('sortProperty and sortDirection must have the sane number of values', HttpStatus.INTERNAL_SERVER_ERROR);
        }
        //let sortSql: string = '';
        //let sortSqlSep: string = '';
        let i: number = 0;
        for(i = 0; i<orderByProps.length; i++) {
            const orderByProp = orderByProps[i];
            let sortProp = sortProperties[orderByProp];
            if (!sortProp) {
                throw new HttpException('sortProperty value unsupported. See possible values.', HttpStatus.INTERNAL_SERVER_ERROR);
            }
            const orderByDir = orderByDirs[i];
            queryBuilder.addOrderBy(sortProp, orderByDir);
            //sortSql += `${sortSqlSep}${sortProp} ${orderByDir}`;
            //sortSqlSep = ',';
        }

        console.log(queryBuilder.getSql());
        const count = await queryBuilder.getCount();
        queryBuilder.take(eGovSearchQuery.limit)
        queryBuilder.skip(eGovSearchQuery.page * eGovSearchQuery.limit)
        const egovCollection = await queryBuilder.getMany();

        return {count: count, query: eGovSearchQuery, results: egovCollection };
    }
}
