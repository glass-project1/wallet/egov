import {Injectable} from "@nestjs/common";
import {EGovUser} from "./egovuser.entity";

@Injectable()
export class EGovUserService {
    async findByEmail(email: string): Promise<EGovUser[]> {
        console.log("EGovUserService.findByEmail EGovUser.email=", email);
        return await EGovUser.find({where: {email: email}, order: {userId: "DESC"}})
    }
}