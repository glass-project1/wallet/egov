export interface EGovIdLockedResponse {

    id: string;

    givenName: string;

    surname: string;

    locked: string | boolean;
}
