import { Connection } from "typeorm";
import { Controller, Get, Put, Param, Body, Post, UseGuards, Query, Request, NotFoundException } from '@nestjs/common';
import { AuthGuard } from "@nestjs/passport";
import { ApiBearerAuth, ApiOperation, ApiTags, ApiParam, getSchemaPath, ApiExtraModels, ApiOkResponse, ApiProperty, ApiResponse } from "@nestjs/swagger";
import { EGovEntity } from './egov.entity';
import { EGovService } from './egov.service';
import { EGovQuery, EGovQueryValidator } from "./egovquery.validator";
import { EGovRepository } from "./egov.repository";
import { PaginatedDto } from "../paginated.dto";
import {GlassDID, GlassWalletManager} from "@glass-project1/glass-toolkit";
import {inject} from "@glass-project1/db-decorators";
import { EGovIdLockedResponse } from "./egovidlockedresponse";



export class EGovCreateNewServiceWalletDto {
    @ApiProperty({ description: "DID schema" })
    didSchema: {};

    @ApiProperty({ description: "Domain name" })
    domain: string;
};

@ApiExtraModels(PaginatedDto)
@ApiTags('EGov')
//@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth()
@Controller('/egov/egov')
export class EGovController {

    private eGovRepository: EGovRepository;

    @inject("GlassWalletManager")
    walletManager!: GlassWalletManager;

    constructor(
        private connection: Connection,
        private eGovService: EGovService
    ) {
        this.eGovRepository = this.connection.getCustomRepository(EGovRepository);
    }

    @Get()
    @ApiOperation({summary: "Search for EGovs based on a query, with paginated results."})
    @ApiOkResponse({
        schema: {
            allOf: [
                { $ref: getSchemaPath(PaginatedDto) },
                {
                    properties: {
                        results: {
                            type: 'array',
                            items: { $ref: getSchemaPath(EGovEntity) },
                        },
                    },
                },
            ],
        },
    })
    async search(@Query(EGovQueryValidator) eGovQuery: EGovQuery): Promise<PaginatedDto<EGovQuery, EGovEntity>> {
        console.log("egov.controller.search... query=", eGovQuery);
        const page = await this.eGovRepository.search(eGovQuery);
        console.log("egov.controller.search results =", page);
        return page;
    }

    @Get(":eGovIdId")
    @ApiOperation({ summary: 'Get one EGov record' })
    @ApiParam({ name: 'eGovIdId', type: String })
    @ApiOkResponse({
        type: EGovEntity
    })
    async findOne(@Param() params): Promise<EGovEntity> {
        console.log("egov.controller.findOne... id=", params.eGovIdId);
        const eGov = await EGovEntity.findOne(params.eGovIdId);
        if (!eGov) throw new NotFoundException(`Not found Egov.eGovIdId="${params.eGovIdId}"`);
        console.log("egov.controller.findOne egov =", eGov);
        return eGov;
    }

    /* don't allow creation of new EGovIds */
    /*
    @Post()
    @ApiOperation({ summary: 'Create one EGovId' })
    async create(@Body() eGovId: EGovId): Promise<EGovId> {
        console.log("egovid.controller.post... egovid=", eGovId);
        await this.eGovIdService.create(eGovId);
        console.log("egovid.controller.post DB connection closed, egovid =", eGovId);
        return eGovId;
    }
    */

    /* don't allow update of EGovIds */
    /*
    @Put() // update all fields ???
    @ApiOperation({ summary: 'Update one EGovId' })
    async update(@Body() eGovId: EGovId): Promise<EGovId> {
        console.log("egovid.controller.put... egovid=", eGovId);
        this.eGovIdService.update(eGovId);
        console.log("egovid.controller.put DB connection closed, egovid =", eGovId);
        return eGovId;
    }
    */

    @Post(":eGovIdId")
    @ApiOperation({ summary: 'Create one wallet for a given ID' })
    @ApiParam({ name: 'eGovIdId', type: String })
    @ApiResponse({
        status: 201,
        description: "An EGovDApp has been created, or already existed for this ID.",
        type: EGovEntity
    })
    async createWallet(@Param() params, @Body() body: any): Promise<EGovEntity|EGovIdLockedResponse> {
        console.log(`egov.controller.post/${params.eGovIdId}...body`, body);

        const eGov = await this.eGovService.createWallet(params.eGovIdId);
    
        console.log("egov.controller.post/${params.id} DB connection closed, egov =", eGov);
        return eGov;
    } 


    @Get("/setup/onetime")
    @ApiOperation({ summary: 'Setup a wallet with a DID for this government agency. No need to invoke explicitely, as it will be invoked internally on the 1st use.' })
    @ApiOkResponse({ description: "The returned string is the agency's DID. (Even if invoked more than once, the agency DID is always the same)."})
    async setup() : Promise<string> {
        console.log(`egov.controller.setup`);


        return this.walletManager.did.getIdentifier();
    }


    @Post("/setup/createNewServiceWallet")
    @ApiOperation({ summary: 'Create one ServiceWallet for a given domain and DID-schema-name. Returns a string with the seedSSI of the new ServiceWallet.' })
    @ApiOkResponse({
        status: 201,
        type: String,
        description: "Created"
    })
    async createNewServiceWallet(@Body() createNewServiceWalletDto: EGovCreateNewServiceWalletDto, @Request() req: any) : Promise<string> {
        console.log(`egov.controller.createNewServiceWallet ${JSON.stringify(createNewServiceWalletDto)}`);

        const serviceWalletSeedSSI = await this.eGovService.createNewServiceWallet(
            createNewServiceWalletDto.didSchema, createNewServiceWalletDto.domain);
    
        console.log("egov.controller.createNewServiceWallet =", serviceWalletSeedSSI);
        return serviceWalletSeedSSI;
    }

}

