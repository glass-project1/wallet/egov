import { ArgumentMetadata, BadRequestException, Injectable, PipeTransform } from "@nestjs/common"
import { IsDateString, IsEnum, IsInt, IsNumber, IsOptional, IsString, Min, validate } from "class-validator"
import { plainToClass, Transform } from "class-transformer"
import { ApiProperty } from "@nestjs/swagger"

@Injectable()
export class EGovQueryValidator implements PipeTransform<EGovQuery> {
    async transform(value: object, metadata: ArgumentMetadata): Promise<EGovQuery> {
        console.log('egovidquery.validator.transform raw=', value)
        const search = plainToClass(metadata.metatype, value)
        const errors = await validate(search, { skipMissingProperties: false, whitelist: true, transform: true })
        if (errors.length > 0) {
            const message = Object.values(errors[0].constraints).join(". ").trim()
            throw new BadRequestException(message)
        }
        console.log('egovidquery.validator.transform return=', search)
        return search
    }
}

export enum EGovQuerySortProperty {
    EGOVID_ID = "eGovIdId",
    DID = "did",
    CREATED_ON = "createdOn"
};
export enum EGovQuerySortDirection {
    ASC = "ASC",
    DESC = "DESC"
};
export class EGovQuery {

    @ApiProperty({ required: false, description: "Filter by exact match to EGov.did" })
    @IsOptional()
    @IsString({ each: true })
    did: string;

    @ApiProperty({ required: false, description: "Filter by exact match to EGov.eGovIdId" })
    @IsOptional()
    @IsString({ each: true })
    eGovIdId: string;

    @ApiProperty({
        type: Date,
        isArray: false,
        example: '1960-12-29',
        required: false, description: "Filter by EGov.createdOn >= (inclusive lower bound)" })
    @IsOptional()
    @IsDateString()
    createdOnStart: Date;

    @ApiProperty({
        type: Date,
        isArray: false,
        example: '2029-12-31',
        required: false, description: "Filter by EGov.createdOn < (exclusive upper bound)" })
    @IsOptional()
    @IsDateString()
    createdOnEnd: Date;

    @ApiProperty({ required: false, description: "Number of items per page. Defaults to 10." })
    @IsOptional()
    @IsInt()
    @Min(1)
    @Transform(({ value }) => parseInt(value))
    limit: number = 10;

    @ApiProperty({ required: false, description: "Page number. Starts at zero. Defaults to zero." })
    @IsOptional()
    @IsInt()
    @Min(0)
    @Transform(({ value }) => parseInt(value))
    page: number = 0;

    @ApiProperty({ required: false, description: "Sort property name. Defaults to 'createdOn'. Possible values are 'eGovIdId', 'did', 'createdOn'." })
    @IsOptional()
    @IsEnum(EGovQuerySortProperty, { each: true })
    sortProperty: EGovQuerySortProperty = EGovQuerySortProperty.CREATED_ON;

    @ApiProperty({ required: false, description: "Sort property order. Use ASC or DESC. Defaults to DESC." })
    @IsOptional()
    @Transform(({ value }) => {
        return value.toUpperCase()
    })
    @IsEnum(EGovQuerySortDirection, { each: true })
    sortDirection: EGovQuerySortDirection = EGovQuerySortDirection.DESC;

};