import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AppResourceController } from './appresource.controller';
import { EGovController } from './egov.controller';
import { EGovService } from './egov.service';
import { EGovUserService } from './egovuser.service';
@Module({
  imports: [HttpModule,
    TypeOrmModule.forRoot({
    "name": "default",
    "type": "postgres",
    "host": ( process.env.EGOVDB_HOST || "localhost" ),
    "port": ( process.env.EGOVDB_PORT ? parseInt(process.env.EGOVDB_PORT) : 5432 ),
    "username": "egov",
    "password": "egov",
    "database": "egov",
    "entities": [
      "dist/egov/*.entity.js"
    ],
    "synchronize": false,
    "logging": true,
    "extra": {
        "poolSize": 27
    }
  })],
  controllers: [
    AppResourceController,
    EGovController,
  ],
  providers: [
    EGovService,
    EGovUserService
  ],
  exports: [
    EGovService,
    EGovUserService
  ],
})
export class EGovModule { }
