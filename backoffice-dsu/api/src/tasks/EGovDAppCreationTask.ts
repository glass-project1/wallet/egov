import { Task } from "@glass-project1/glass-toolkit/lib/providers/concurrency/Task";
import {DSU, KeySSI} from "@glass-project1/opendsu-types";
import {
    BasicEnvironmentDefinition,
    getToolkitInjectables,
    GlassWalletManager, IEGovDApp,
    MarketKeys
} from "@glass-project1/glass-toolkit";
import {BlueprintOpenDSURepository} from "@glass-project1/dsu-blueprint";
import {EGovDApp} from "@glass-project1/glass-toolkit/lib/blueprints/eGovernance/EGovDApp";
import {inject} from "@glass-project1/db-decorators";
import {debug, Err, errorCallback} from "@glass-project1/logging";
import {generateWalletEnvironment} from "@glass-project1/glass-toolkit/lib/providers";

export type EGovDAppCreationModel = {
    id: string,
    passport: string,
    criminal?: string,
    birth?: string,
    tax?: string,
    health?: string,
    education?: string,
    environment: BasicEnvironmentDefinition,
    did: {
        data: {
            name: string,
            identifier: string,
            publishes?: string[]
        }
    },
    marketDomain: string
}

export type EGovDAppCreationResult = {
    walletSSI: string,
    walletDID: string
}

export default class EGovDAppCreationTask  implements Task<EGovDAppCreationModel, EGovDAppCreationResult> {
    name = "EGovDAppCreationTask";

    @inject("GlassWalletManager")
    wm!: GlassWalletManager;

    execute(data: EGovDAppCreationModel): Promise<EGovDAppCreationResult> {
        const self = this;
        return new Promise((resolve, reject) => {
            const {id, passport, criminal, birth, tax, health, education, environment, did, marketDomain} = data;
            const keyCache = {}
            keyCache[MarketKeys.DOMAIN] = marketDomain; //sets the market domain flag

            const eGovDAppDSU = EGovDApp(getToolkitInjectables(), {
                id: id,
                passport: passport,
                criminal: criminal,
                birth: birth,
                tax: tax,
                health: health,
                education: education,
                environment: generateWalletEnvironment("egov-dapp", environment),
                did: did,
                code: "egov-dapp"
            });

            const repo = new BlueprintOpenDSURepository(EGovDApp, getToolkitInjectables(), environment.domain, this.wm.did, undefined, keyCache);

            debug.call(self, "Creating new Egovernance DApp");
            repo.create(eGovDAppDSU, (err: Err, eGovDApp?: IEGovDApp, dsu?: DSU, keySSI?: KeySSI) => {
                debug.call(self,`repo.create eGovDAppDSU returned {0}, {1}, {2}`, err, eGovDApp, keySSI ? keySSI.getIdentifier(true) : "No ssi");
                if (err || !eGovDApp || !dsu || !keySSI)
                    return errorCallback.call(self, "Failed to create eGovDApp: {0}", reject, err || "Missing results")

                resolve({
                    walletSSI: keySSI.getIdentifier(true),
                    walletDID: eGovDApp.did.getIdentifier()
                })
            })
        })
    }

}