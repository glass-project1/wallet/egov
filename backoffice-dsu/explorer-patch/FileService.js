function FileService() {

  this.environment = undefined;

  this.constructUrlBase = function (prefix) {
    let url;
    let location = window.location;
    const paths = location.pathname.split("/");
    while (paths.length > 0) {
      if (paths[0] === "") {
        paths.shift();
      } else {
        break;
      }
    }
    let applicationName = paths[0];
    prefix = prefix || "";
    url = `${location.protocol}//${location.host}/${prefix}${applicationName}`;
    return url;
  }

  this.createRequest = function (url, method, callback) {
    let xhr = new XMLHttpRequest();
    xhr.open(method, url);
    xhr.onload = function () {
      if (xhr.status != 200) {
        callback(`Error ${xhr.status}: ${xhr.statusText}`);
      } else {
        callback(undefined, xhr.response);
      }
    };
    xhr.onerror = function () {
      callback("Request failed");
    };
    return xhr;
  }

  this.getFromMarket = function (appId, callback){
    const self = this;

    if (!this.environment)
      return this.getEnvironment((e) => {
        if (e)
          return callback(e);
        self.getFromMarket(appId, callback);
      });

    const domain = this.environment.domain || this.environment.vaultDomain || "vault";

    const location = window.location;
    const url = encodeURI(`${location.protocol}//${location.host}/market/${domain}/app/${appId}`);
    this.createRequest(url, "GET", (err, response) => {
      if (err)
        return callback(err);
      try {
        response = JSON.parse(response);
      } catch (e) {
        return callback(e);
      }
      if (!response.keySSI)
        return callback("missing keySSI from response from market")

      callback(undefined, response.keySSI)
    }).send();
  }

  this.getEnvironment = function(callback){
    this.getFile("loader/environment.js", (err, environment) => {
      if (err)
        return callback(err);
      try {
        this.environment = JSON.parse(environment.replace("export default ", ""));
      } catch (e){
        return callback(e);
      }
      callback(undefined, this.environment);
    })
  }

  this.getFile = function (url, callback) {

    if (url.startsWith("wallet-patch") && url.endsWith("seed"))
      return this.getFromMarket("dsu-explorer-wallet-PDM&FC", callback);
    if (url.startsWith("apps-patch") && url.endsWith("seed"))
      return this.getFromMarket("dsu-explorer-ssapp-PDM&FC", callback);

    url = this.constructUrlBase() + "/" + url;
    this.createRequest(url, "GET", callback).send();
  };

  this.getFolderContentAsJSON = function (url, callback) {
    if (url.startsWith("apps-patch"))
      return this.getFromMarket("dsu-explorer-ssapp-PDM&FC", (err, seed) => {
        if (err)
          return callback(err);
        callback(undefined, JSON.stringify({"dossier-explorer-ssapp": {
            seed: seed
          }}));
      });

    if (url.startsWith("wallet-patch"))
      return this.getFromMarket("dsu-explorer-wallet-PDM&FC", (err, seed) => {
        if (err)
          return callback(err);
        callback(undefined, JSON.stringify({"/": {
            seed: seed
          }}));
      });

    url = this.constructUrlBase("directory-summary/") + "/" + url;
    this.createRequest(url, "GET", callback).send();
  }

}
export default FileService;
