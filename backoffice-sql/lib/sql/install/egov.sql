--
-- PostgreSQL database dump
--

-- Dumped from database version 12.12 (Ubuntu 12.12-1.pgdg22.04+1)
-- Dumped by pg_dump version 12.12 (Ubuntu 12.12-1.pgdg22.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: appresource; Type: TABLE; Schema: public; Owner: egov
--

CREATE TABLE public.appresource (
    id bigint NOT NULL,
    key character varying(256) NOT NULL,
    locale character varying(5),
    value text NOT NULL,
    help text
);


ALTER TABLE public.appresource OWNER TO egov;

--
-- Name: TABLE appresource; Type: COMMENT; Schema: public; Owner: egov
--

COMMENT ON TABLE public.appresource IS 'Arc - operational parameters or messages/resources that may need translation';


--
-- Name: COLUMN appresource.id; Type: COMMENT; Schema: public; Owner: egov
--

COMMENT ON COLUMN public.appresource.id IS 'id - PK';


--
-- Name: COLUMN appresource.key; Type: COMMENT; Schema: public; Owner: egov
--

COMMENT ON COLUMN public.appresource.key IS 'key - resource key';


--
-- Name: COLUMN appresource.locale; Type: COMMENT; Schema: public; Owner: egov
--

COMMENT ON COLUMN public.appresource.locale IS 'locale - if NULL then this resource is not translateable, and should be considered a configuration parameter.';


--
-- Name: COLUMN appresource.value; Type: COMMENT; Schema: public; Owner: egov
--

COMMENT ON COLUMN public.appresource.value IS 'value - value of the resource. Set to "?" to represent absence.';


--
-- Name: COLUMN appresource.help; Type: COMMENT; Schema: public; Owner: egov
--

COMMENT ON COLUMN public.appresource.help IS 'help - help message describing the parameter or resource';


--
-- Name: appresource_id_seq; Type: SEQUENCE; Schema: public; Owner: egov
--

CREATE SEQUENCE public.appresource_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.appresource_id_seq OWNER TO egov;

--
-- Name: appresource_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: egov
--

ALTER SEQUENCE public.appresource_id_seq OWNED BY public.appresource.id;


--
-- Name: egov; Type: TABLE; Schema: public; Owner: egov
--

CREATE TABLE public.egov (
    egovidid character varying(20) NOT NULL,
    did character varying(4096) NOT NULL,
    walletkeyssi text,
    createdon timestamp with time zone NOT NULL
);


ALTER TABLE public.egov OWNER TO egov;

--
-- Name: TABLE egov; Type: COMMENT; Schema: public; Owner: egov
--

COMMENT ON TABLE public.egov IS 'eGov - All eGov wallets issued so far.';


--
-- Name: COLUMN egov.did; Type: COMMENT; Schema: public; Owner: egov
--

COMMENT ON COLUMN public.egov.did IS 'did - DID identified of the EGovDApp issued.';


--
-- Name: COLUMN egov.egovidid; Type: COMMENT; Schema: public; Owner: egov
--

COMMENT ON COLUMN public.egov.egovidid IS 'egovidid - citizenship identification number. For now it is mandatory, but in really could be a one of eGovIdId, eGovTaxId, eGovSSN.';


--
-- Name: COLUMN egov.walletkeyssi; Type: COMMENT; Schema: public; Owner: egov
--

COMMENT ON COLUMN public.egov.walletkeyssi IS 'walletkeyssi - seed SSI of the created EGovDApp DSU.';


--
-- Name: COLUMN egov.createdon; Type: COMMENT; Schema: public; Owner: egov
--

COMMENT ON COLUMN public.egov.createdon IS 'createdOn - Timestamp when the wallet was issued. Needed to sort records by time of creation.';



--
-- Name: egovuser; Type: TABLE; Schema: public; Owner: egov
--

CREATE TABLE public.egovuser (
    userid uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    email character varying(100) NOT NULL,
    passhash character varying(100) NOT NULL
);


ALTER TABLE public.egovuser OWNER TO egov;

--
-- Name: TABLE egovuser; Type: COMMENT; Schema: public; Owner: egov
--

COMMENT ON TABLE public.egovuser IS 'Eu - EGovUser - a generic user, access right will depend to which entities is the user associated';


--
-- Name: COLUMN egovuser.userid; Type: COMMENT; Schema: public; Owner: egov
--

COMMENT ON COLUMN public.egovuser.userid IS 'userid - primary key';


--
-- Name: COLUMN egovuser.email; Type: COMMENT; Schema: public; Owner: egov
--

COMMENT ON COLUMN public.egovuser.email IS 'email - not null, user mail';


--
-- Name: COLUMN egovuser.passhash; Type: COMMENT; Schema: public; Owner: egov
--

COMMENT ON COLUMN public.egovuser.passhash IS 'passHash - password hash';


--
-- Name: locale; Type: TABLE; Schema: public; Owner: egov
--

CREATE TABLE public.locale (
    code character varying(5) NOT NULL,
    description character varying(200)
);


ALTER TABLE public.locale OWNER TO egov;

--
-- Name: TABLE locale; Type: COMMENT; Schema: public; Owner: egov
--

COMMENT ON TABLE public.locale IS 'Loc - Locales. Does not include encoding.';


--
-- Name: COLUMN locale.code; Type: COMMENT; Schema: public; Owner: egov
--

COMMENT ON COLUMN public.locale.code IS 'code - locale code. Ex: "en", "en_US"';


--
-- Name: COLUMN locale.description; Type: COMMENT; Schema: public; Owner: egov
--

COMMENT ON COLUMN public.locale.description IS 'description - a desription of the locale';


--
-- Name: appresource id; Type: DEFAULT; Schema: public; Owner: egov
--

ALTER TABLE ONLY public.appresource ALTER COLUMN id SET DEFAULT nextval('public.appresource_id_seq'::regclass);


--
-- Data for Name: appresource; Type: TABLE DATA; Schema: public; Owner: egov
--

COPY public.appresource (id, key, locale, value, help) FROM stdin;
1	egov.version	\N	0.5.1	RDBMS Schema version
2	egov.wallet.credentials	\N	["agency-egov-secret-wallet-password","more-secret"]	Agency wallet secret credentials as a JSON array of strings.
3	egov.wallet.didIdentifier	\N	?	Once the agency wallet has been initialized, this parameter is replaced by the DID identifier string.
4	egov.dapp.pt.keyTemplate	\N	{"countryCode": "pt","name": "eGovernance Portugal", "description": "Aceda aqui a toda a sua documentação governamental", "publisher": "eGov PT", "version": "The DApp version string", "icon": "accessibility", "keySSI": "_KEYSSI_TEMPLATE_"}	JSON template for the data needed to open the DApp.
5	egov.dapp.gr.keyTemplate	\N	{"countryCode": "gr","name": "eGovernance Greece", "description": "Αποκτήστε πρόσβαση σε όλα τα κρατικά σας έγγραφα εδώ", "publisher": "eGov GR", "version": "The DApp version string", "icon": "accessibility", "keySSI": "_KEYSSI_TEMPLATE_"}	JSON template for the data needed to open the DApp.
6	egov.dapp.tk.keyTemplate	\N	{"countryCode": "tk","name": "eGovernance Turkey", "description": "Tüm devlet belgelerinize buradan erişin", "publisher": "eGov TK", "version": "The DApp version string", "icon": "accessibility", "keySSI": "_KEYSSI_TEMPLATE_"}	JSON template for the data needed to open the DApp.
7	egov.egovid.borestUrl	\N	http://localhost:3001/borest	URL prefix to call eGovId backoffice REST services. Do not change the pre-set default value without changing the backoffice-sql/lib/install/egov_run.sh.
8	egov.egovpassport.borestUrl	\N	http://localhost:3002/borest	URL prefix to call eGovPassport backoffice REST services. Do not change the pre-set default value without changing the backoffice-sql/lib/install/egov_run.sh.
9	egov.egovcriminalrecord.borestUrl	\N	http://localhost:3001/borest	URL prefix to call eGovCriminalRecord backoffice REST services. Do not change the pre-set default value without changing the backoffice-sql/lib/install/egov_run.sh.
10	egov.egovbirthcertificate.borestUrl	\N	http://localhost:3001/borest	URL prefix to call eGovBirthCertificate backoffice REST services. Do not change the pre-set default value without changing the backoffice-sql/lib/install/egov_run.sh.
11	egov.egovtax.borestUrl	\N	http://localhost:3003/borest	URL prefix to call eGovTax backoffice REST services. Do not change the pre-set default value without changing the backoffice-sql/lib/install/egov_run.sh.
12	egov.egovhealth.borestUrl	\N	http://localhost:3004/borest	URL prefix to call eGovHealth backoffice REST services. Do not change the pre-set default value without changing the backoffice-sql/lib/install/egov_run.sh.
13	egov.egoveducation.borestUrl	\N	http://localhost:3005/borest	URL prefix to call eGovEducation backoffice REST services. Do not change the pre-set default value without changing the backoffice-sql/lib/install/egov_run.sh.
\.


--
-- Data for Name: egov; Type: TABLE DATA; Schema: public; Owner: egov
--

COPY public.egov (egovidid, did, walletkeyssi, createdon) FROM stdin;
\.


--
-- Data for Name: egovuser; Type: TABLE DATA; Schema: public; Owner: egov
--

COPY public.egovuser (userid, email, passhash) FROM stdin;
744b02da-ee30-46dd-b894-35218029e87e	admin@someagency.gov	123456
06127849-9b1c-41fa-8461-4fac6eeb1d97	username@someagency.gov	123456
\.


--
-- Data for Name: locale; Type: TABLE DATA; Schema: public; Owner: egov
--

COPY public.locale (code, description) FROM stdin;
en	en
en_US	en_US
pt	pt
pt_BR	pt_BR
pt_PT	pt_PT
en_GB	en_GB
\.


--
-- Name: appresource_id_seq; Type: SEQUENCE SET; Schema: public; Owner: egov
--

SELECT pg_catalog.setval('public.appresource_id_seq', 9, true);


--
-- Name: appresource pk_appresource_id; Type: CONSTRAINT; Schema: public; Owner: egov
--

ALTER TABLE ONLY public.appresource
    ADD CONSTRAINT pk_appresource_id PRIMARY KEY (id);


--
-- Name: egov pk_egov_did; Type: CONSTRAINT; Schema: public; Owner: egov
--

ALTER TABLE ONLY public.egov
    ADD CONSTRAINT pk_egov_egovidid PRIMARY KEY (egovidid);


--
-- Name: locale pk_locale_code; Type: CONSTRAINT; Schema: public; Owner: egov
--

ALTER TABLE ONLY public.locale
    ADD CONSTRAINT pk_locale_code PRIMARY KEY (code);


--
-- Name: egovuser pk_users_userid; Type: CONSTRAINT; Schema: public; Owner: egov
--

ALTER TABLE ONLY public.egovuser
    ADD CONSTRAINT pk_users_userid PRIMARY KEY (userid);


--
-- Name: egovuser unq_egovuser_email; Type: CONSTRAINT; Schema: public; Owner: egov
--

ALTER TABLE ONLY public.egovuser
    ADD CONSTRAINT unq_egovuser_email UNIQUE (email);


--
-- Name: appresource fk_appresource_locale; Type: FK CONSTRAINT; Schema: public; Owner: egov
--

ALTER TABLE ONLY public.appresource
    ADD CONSTRAINT fk_appresource_locale FOREIGN KEY (locale) REFERENCES public.locale(code);


--
-- PostgreSQL database dump complete
--

