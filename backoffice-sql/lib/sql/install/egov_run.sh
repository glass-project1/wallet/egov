#!/bin/bash -x
# this is used by ../docker/docker-compose.yml
# it runs (inside the docker only) the egov.sql script with the user egov
set -e
# remove ON_ERROR_STOP=1 to ignore errors
#psql -v ON_ERROR_STOP=1 --username "egov" --dbname "egov" -f /docker-entrypoint-initdb.d/60egov.norun
# removed because there is an error on a COMMENT, ERROR:  must be owner of extension uuid-ossp
psql -v --username "egov" --dbname "egov" -f /docker-entrypoint-initdb.d/60egov.norun
if [ -n "$GLASS_COUNTRY" ]
then
    if [[ ! "$GLASS_COUNTRY" =~ [A-ZA-Z] ]] ; then echo 2>&1 "env GLASS_COUNTRY must be an ISO 2 letter code upper-case" ; exit 1 ; fi
    GLASS_COUNTRY_LC=$(echo "$GLASS_COUNTRY" | tr '[:upper:]' '[:lower:]')
    psql --echo-all -v --username "egov" --dbname "egov" <<EOF
UPDATE appresource
    SET value='http://egov-id-${GLASS_COUNTRY_LC}/borest'
    WHERE key='egov.egovid.borestUrl';
UPDATE appresource
    SET value='http://egov-passport-${GLASS_COUNTRY_LC}/borest'
    WHERE key='egov.egovpassport.borestUrl';
UPDATE appresource
    SET value='http://egov-id-${GLASS_COUNTRY_LC}/borest'
    WHERE key='egov.egovcriminalrecord.borestUrl';
UPDATE appresource
    SET value='http://egov-id-${GLASS_COUNTRY_LC}/borest'
    WHERE key='egov.egovbirthcertificate.borestUrl';
UPDATE appresource
    SET value='http://egov-tax-${GLASS_COUNTRY_LC}/borest'
    WHERE key='egov.egovtax.borestUrl';
UPDATE appresource
    SET value='http://egov-health-${GLASS_COUNTRY_LC}/borest'
    WHERE key='egov.egovhealth.borestUrl';
UPDATE appresource
    SET value='http://egov-education-${GLASS_COUNTRY_LC}/borest'
    WHERE key='egov.egoveducation.borestUrl';
EOF
else
    echo 2>&1 "WARNING: environment variable GLASS_COUNTRY not set! Preserving default appresource URLs" 
fi
