# sql/install

The scripts under this install folder are supposed to run only once, when the database is setup for the 1st time.

## Files

```sh
setup.sql - create user and databse egov (to run as PostgreSQL superuser - typically postgres)
egov.sql - create and populate egov database (to run as egov user)
egov_run.sh - to be used from the docker. Do not run manually.
```