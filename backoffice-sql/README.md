# backoffice-sql

This folder has a conventional RDBMS (SQL) database that holds a record of all eGovernment wallets issued by the Ministry of Digital Governance.


## Setup local PostgreSQL database

For developers that need to make a change to this database, read this section to setup a development environment.

### Install PostgreSQL client+server+contrib+libpq

You will probably need a native PostgreSQL installation. These instructions are for a debian derived distro (such as Ubuntu) - but they could be adapted to any other environment supporting PostgreSQL.

On a Linux (debian based release), read PROJECT-ROOT/backoffice-sql/lib/sql/install/setup.sh
(adapt it with another name, if needed), and execute it as root.
On Windows, please see download and installation instructions on http://www.postgresql.org

This will install PostgreSQL, client and server (including the native libpq client).

PostgreSQL version >= 12.9

(It should work fine on any version above 12.9, but please contact the team before changing the PROJECT-ROOT/backoffice-sql/lib/sql/install/egov.sql file to support a more recent version only.)


### Instructions to create a new PostgreSQL egov database

As of now, you should have a local PostgreSQL server running.
(These instructions assume a Linux environment, but a Windows environment
will be equivalent: without the sudo command ; path-name slashes "/" changed for "\" ;
the psql.exe binary is in the execution PATH ; and that the postgres
super-user is named "postgres" as usual. In Windows you will need to know the
postgres super-user password, setup on installation.)

As root, type

```bash
sudo su - postgres
cd PROJECT-ROOT/backoffice-sql/lib/sql/install
psql postgres postgres
\i setup.sql
\q
```

(If user postgres does not have privileges to read your project files, just past setup.sql into psql one line at a time).

This will create a Postgres user egov, password egov,
owner of a database named egov. But the database will be empty
(without any tables and without any data), and needs to be populated.

### Instructions to populate the PostgreSQL egov database

Using your regular shell user, feed the SQL file
PROJECT-ROOT/backoffice-sql/lib/sql/install/egov.sql
into the psql command-line client:

```bash
cd PROJECT-ROOT/backoffice-sql/lib/sql/install
psql --host=localhost egov egov
drop owned by egov; -- not needed on initial creation
\i egov.sql
\q
```

PS: The --host=localhost is to prevent the peer authentication issue
https://stackoverflow.com/questions/18664074/getting-error-peer-authentication-failed-for-user-postgres-when-trying-to-ge


## Database First

This project works in "database first" mode. This means that, for any change/addition to the database, they are first developed on the SQL side, and only afterwards on the TypeScript entities.

### Before Production Release

No need for database migrations.
Just edit the PROJECT-ROOT/backoffice-sql/lib/sql/install/egov.sql file, and re-create the database as needed.
(Please attempt to keep existing user's data. Communicate with the users when there are questions about what to keep/discard between releases).

### After Production Release

For each incremental change there is an associated database migration. See "Upgrade Conventions" below.

### Data Model Editor

This is no "official data model editor". The PROJECT-ROOT/backoffice-sql/lib/sql/install/egov.sql file is the data-model and initial data.

You may use a tool such as the "Community Edition" of
https://dbschema.com/
to change the database, and then generate an updated egov.sql file
with the command

```bash
pg_dump --host=localhost -U egov > PROJECT-ROOT/backoffice-sql/lib/sql/install/egov.sql
```

Please communicate with your team the update of the data model, and take care
do not commit data improper for a public repository.





## Schema conventions (to create new tables and columns)


Recomended SQL types:

```sql
    TEXT         - for strings without well-know maxlength.
    VARCHAR(n)   - for strings with known maxlength n.
    INTEGER      - for 32bit integer
    BIGINTEGER   - for 64bit integer
    TIMESTAMPTZ  - for date & time stamps.
```

### Naming Conventions for Tables


Table names are required to be classified as nouns in
singular form. Nouns can be concatenated. 

Abbreviation rules in codingConventions apply.

All code (SQL or JS) should be treated as case sensitive.

Good Examples:

```sql
    Customer     
    Film         
    CustomerStatus 
```

Bad Examples:

```sql
    Customers     -- plural
    InsertFilms   -- "Insert" is a verb. Its not a table creation script.
    StatusOfCustomer -- nouns are concatenated (CustomerStatus is better).
```

Tables that are used to represent associations between 2 or more
tables have their names derived from the name of the relationship,
which also follows conventions for Class names.

Good Example:
```sql
     CustomerBillingAddress -- relation between Customer and Address
                            -- for billing purposes.
```

Every table should (should != must) have a documented alias. An alias
is a short 1 to 6 letter name which identifies the table uniquely
in the context of other possibly related tables.

Globally unique aliases are hard to maintain for medium/large DB
schema (medium = > 100 tables), so uniqueness is only required in
the context of other tables to which this table can be related to.

The "official" alias should be documented in the DB table
comments in the following format:

```
    <capitalized table name>(<alias>) - <comment on table>
```

Example:
    Table Customer
    Alias Cst

    The comment on the table should be:

```sql
    COMMENT ON TABLE Customer 
    IS 'Customer(Cst) - Information about the customer';
```

    Note the left part of the " - " where the capitalized name of the
    table and the alias can be fetched by the code generatio utility!

Also the comments on columns should use the same approach in order to 

have the correct capitalized format:

```
    <capitalized column name> - <comment on column>
```

Example:

    Table Customer
    Column accountNumber

    The comment on the column should be:

```sql
    COMMENT ON COLUMN Customer.accountNumber
    IS 'accountNumber - The account number for the customer in the format XX.';
```

Good Example:

```sql
    Address alias = adr
    Customer alias = cst
    CustomerBillingAddress alias = cba 
```

Bad Example:

```
    LocalFilmDistributor alias = x01
```

The aliases are intended to be used in "SELECT" queries where
several tables are involved with identical column names, to
distinguish columns.

See "SELECT Guidelines" for illustration on the
use of alias.

### Naming Conventions for Columns

The names for columns follow the coding convention names for attribute
names of classes.

The name of a foreign key is usually the name of the referenced table
unless the role of the reference has a different name, or the
referenced table name already has redundant nouns on the current
context.

Example (in C++ syntax):

```cpp
class Customer {
   int             id;
   Country        *country;
   Address        *shippingAddress;
   Address        *billingAddress;
   CustomerStatus *status;
};

CREATE TABLE Customer {
   id              NUMBER PRIMARY KEY,
   country         INTEGER REFERENCES Country,
   shippingAddress INTEGER REFERENCES Address,
   billingAddress  INTEGER REFERENCES Address,
   status          CHAR(1) REFERENCES CustomerStatus
};
```
   

### Primary Keys

In the general case, the primary key column is always called "id" and
is a 64 bit integer (java Long / JS number only works well up to 56 bit integers).
Ids take positive values, starting on 1,
and growing. (It is not guaranteed the the increment is one. Just that
they are UNIQUE and always growing).

The recommended Postgres SQL data type is BIGSERIAL, which will create
automatically a sequence with name

   table_id_seq

and assign the default value of the next sequence value
for the column named "id".

(Most database systems already provide a unique "row id" which could be
used for this purpose, but as this concept is not much portable across
databases, it has been ignored.)

Id values for business objects are not usually visible to the final
customer or to the un-specialized operator.

This means that a business object is usually identified by another
UNIQUE visible key, usually a "Name". (It can also be an email, or
some other attribute identifying the business object uniquely).

These "Name" attributes are normally used as primary keys when
exporting/importing data to other systems (such as spread-sheet
tables). The same business object represented in 2 different systems
will have the same "Name" but can have a different internal id value.

Please also note that "Names" are not always UNIQUE by themselves.
Example: Suppose that a Customer is identified by its Email address.

(The Email is its "Name" attribute).

The customer can leave the account (its status changes to
suspended/terminated), but later joins again in another account.

The second adherence to the system will probably be done with
a different Customer.id, but the Email is no longer UNIQUE as
there is a terminated customer with the same Email.

(Such sophisticated constraint checking requires special triggers
for doing so, or auxiliary tables to enforce this kind of checking.)
Id values can sometimes be made public. For example, if there is not
visible way to identify an "Account", then use the id value as public
account number, instead of making up another visible key and matching
it to internal id values.

(
Exposing id values to the public, also exposes information on the
number of business objects of that class created so far. So the
customers can suspect if they are new. Creating another visible
primary key, which uses a pseudo-random number sequence is a solution,
but it has been abandoned in IteaBackofficeTemplate so far as it was a too big effort for
so Little gain.

Also, internal id value manipulation mechanisms must be robust enough
to external attempts to tampering with id values - ie. to get unauthorized
access to other accounts.
)

For tables which describe limited possible choices for attributes,
such as CustomerStatus, the primary key is usually a CHAR(1)
with a one-char code for each choice, and the second is a
"Description" field. There is no need for a 64-bit id as the number of
records is very limited and never grows.

Example:

```sql
   CustomerStatus.code CustomerStatus.description
                  'A'  Active
                  'S'  Suspended
                  'T'  Terminated
```

### Implementing Inheritance

Please try to keep the model to single inheritance, and limit the use
of inheritance to when you really need it. (If you can solve the
modeling problem with E/R techniques without any special tricks or
constraints, then there is possibly no need to use inheritance).

Database object extensions are NON-STANDARD and are therefore ignored
for this project.

Using C++ like syntax, the following is a typical mapping of classes
on to relational tables.

```cpp
class Entity { // Root class - an abstract unique entity
   int  id;
   char name[64]; // Unique name.
};

class Customer : Entity { // A customer
   Address *billingAddress;
   float    balance;
};

class CustomerPortuguese : Customer {
   char fiscalId[9]; // Fiscal number required by Portuguese law
};

class CustomerSwedish : Customer {
   char socialId[23]; // Something required specially for Swedish customers. 
};

class Supplier : Entity {
   char contactPhone[32]; 
};
```

SQL mapping "by the book" is:

```sql
CREATE TABLE EntityClass (
   code CHAR(2) PRIMARY KEY,
   name :Name NOT NULL UNIQUE

);

-- Values for possible derived classes
-- '01' - Entity
-- '02' - Customer
-- '03' - CustomerPortuguese
-- '04' - CustomerSwedish
-- '05' - Supplier

CREATE TABLE Entity (
   id    SERIAL PRIMARY KEY,
   class CHAR(2) NOT NULL REFERENCES EntityClass,
   name  :Name  NOT NULL UNIQUE
);

CREATE TABLE Customer (
   id      INTEGER PRIMARY KEY REFERENCES Entity,
   address INTEGER REFERENCES Address,
   balance FLOAT
);

CREATE TABLE CustomerPortuguese (
   id       INTEGER PRIMARY KEY REFERENCES Customer,
   fiscalId VARCHAR(9) NOT NULL UNIQUE
);

CREATE TABLE CustomerSeedish (
   id       INTEGER PRIMARY KEY REFERENCES Customer,
   socialId VARCHAR(32) NOT NULL UNIQUE
);

CREATE TABLE Supplier (
   id           INTEGER PRIMARY KEY REFERENCES Entity,
   contactPhone VARCHAR(32) NOT NULL UNIQUE
);

-- Example of inserting a CustomerPortuguese record and an address.

INSERT INTO Address (...) VALUES (...);
INSERT INTO Entity (class, name)

SELECT code, 'Joao Luis' FROM EntityClass WHERE name='CustomerPortuguese';

INSERT INTO Customer (id, address, balance)
VALUES (CURRVAL(Entity_id_seq), CURRVAL(Address_id_seq), 0.0);

INSERT INTO CustomerPortuguese (id, fiscalId)
VALUES (CURRVAL(Entity_id_seq), '123456789');

-- Example of listing Customers (Portuguese and Sweedish)
-- and getting their details in one query.
-- (OUTER JOINs are allowed in 7.2.x or greater)

  SELECT E.id, E.name, Cst.balance, Csp.fiscalId, NULL
  FROM Entity E, Customer Cst, CustomerPortuguese Csp
  WHERE E.id = Cst.id
    AND Cst.id = Csp.id
UNION 
  SELECT E.id, E.name, Cst.balance, NULL, Css.socialId
  FROM Entity E, Customer Cst, CustomerSwedish Css
  WHERE E.id = Cst.id
    AND Cst.id = Css.id
ORDER BY
  E.name, E.id;

-- colum 4 is NOT NULL for Portuguese, and column 5 is NOT NULL
-- for Swedish.
```

Note that the Entity.class column is equivalent to having run-time type
information. If a query reads a base class record, it can test the
.class column to see which derived class table record must also be
read (if a specific derived class attribute is needed).

Single inheritance is very restrictive. It is usually recommended to look
at special attributes for derived classes as specific property
attributes stored in a separate table. If the record with the same id
exists, then the property exists. (Equivalent to INFOLOG methodology
properties - non-orthogonal).

Please note that Postgres 7.0.x does not support OUTER JOINs, which is
very useful to read specific attributes from all possible derived (in
one query row) classes without knowing the class name in advance.
For this reason, for small attribute extensions, a single base class
table can be used to represent attributes of all derived classes. The
attributes which belong to derived classes can be NULL and are only
filled up when needed.

### General SQL Code Guidelines

This guidelines apply under Java or under .sql files.
Write case sensitive SQL code, just in case :-)

Postgres or Oracle are not case sensitive, but MySql on UNIX is, and
we can never say never... 

#### SELECT Guidelines

SELECT statements which may return more than one result
must always have an "ORDER BY" constraint, such that results
are always given in deterministic order.

(Future versions of Postgres might not give deterministic
results under multi-processor architectures. Oracle doesn't).

(A SELECT with non-deterministic results prevents the use of stateless
techniques for processing query results. Example: On Google searches,
when presenting results in windows of 10 results per page, the next
10 results can be obtained by just repeating the query, and skipping
the first 10 results.)

Good Example:

```sql
    SELECT Cst.firstName, Adr.line1
    FROM Customer AS Cst,
         Address AS Adr,
         CustomerBillingAddress AS Cba
    WHERE
        Cst.id = Cba.customer
        Adr.id = Cba.address
    ORDER BY
        Cst.lastName, Cst.id
```

#### INSERT Guidelines

Always explicitly name the columns of inserted values.
Never rely on the order of the columns when the table was created.

Rely on column default values whenever they are provided, and appropriate.

Good Example:

```sql
    INSERT INTO Customer (firstName, lastName)
    VALUES ('Joao', 'Luis')
```

Bad Example:

```sql
    INSERT INTO Customer
    VALUES (CURRVAL('Customer_id_seq'), 'A', 'Joao', 'Luis')
```




## Upgrade Conventions

NOT IN SCOPE - Only used for production environments

This section is dedicated to describe the necessary procedures
and conventions to follow when developing new features/corrections
that require updating the DB schema of the system in
production.

Every set of SQL scripts to the database should be under

sql/changes/

For each release there is a top-level installation
script named for the next version. For example, for v0.90.0:

sql/changes/sqlV0.90.0.sh

Optional script argument --local | --DEV | --TST | --PRD
specifies target environment. (Defaults to "--local").

Replace the "0.90.0" with the release number.
Each of these release scripts install whatever set of changes have
been selected for that release, since the previous one.

Roll-forward changes are under folders named

```sh
do_NNNNN_IIIII_description/
```

NNNNN is the DbChange.id and is pre-allocated in the master branch

before the development starts.

IIIII is the issue number. (There may be more than one 

DbChange for one issue, but not the other way around).

Do no forget to update table DbChange so that the database
reflects all the applied patches.

Roll-backwards changes are under folders named

```sh
undo_NNNNN_IIIII_description/
```

The sql/install/ script STILL WORK and
create a blank database with all the required "do"s.

The resulting version is documented by parameter

AppResource.key='db-version'

The sql/install/ also contains all
the "do"s that are incorporated on to the 
sql/install/egov.sql

A new developer is supposed to have a blank development database, so
he should use sql/install/egov.sql
setup that database.

TST and PRD evolve incrementally, and should have the "do"s applied
incrementally on every release (by the sqlVN.NN.N.sh script).

To be able to test the "do" scripts, it is possible to go back to an older
release tag VN.NN.N, re-create a blank database, and then go the next
release tag VN.NN+1.N and run the

sql/changes/sqlVN.NN+1.N.sql 

upgrade script.

So, a new development typically has to create the following new files:

```sh
do_NNNNN_IIIII_description/feature.sql

undo_NNNNN_IIIII_description/feature.sql
```
