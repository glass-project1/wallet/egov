#!/bin/bash -xe
# Tool to reset (code and database) in egov-id
MY_PWD=${PWD##*/}          # to assign to a variable
MY_PWD=${MY_PWD:-/}        # to correct for the case where PWD=/
if [ "$MY_PWD" != egov ]
then
    echo 1>&2 "Current working dir must be egov"
    exit 1
fi
rm -rf *
git checkout .
git pull
cd backoffice-sql
PGPASSWORD=egov psql -h localhost egov egov <<EOF
DROP OWNED BY egov;
\i lib/sql/install/egov.sql
EOF
cd ../backoffice-dsu
npm install
npm run install-source
npm run build-source
cd api
ENV_SUFFIX=.local GLASS_COUNTRY=PT GLASS_COUNTRY_NAME=Portugal MARKET_DOMAIN=default DOMAIN=egov.pt GLASS_ENDPOINT=http://localhost:8080 npm run start:dev

