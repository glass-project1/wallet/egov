#!/bin/bash -xei
# Tool to re-build and re-publish all DApps so far in egov
MY_PWD=${PWD##*/}          # to assign to a variable
MY_PWD=${MY_PWD:-/}        # to correct for the case where PWD=/
if [ "$MY_PWD" != egov ]
then
    echo 1>&2 "Current working dir must be egov"
    exit 1
fi
cd ../
(cd glass-wallet-workspace/glass-wallet ; npm run build ; npm run build:dsu ; npm run publish:dsu)
#(cd egov-id/backoffice-dsu/egovid-dapp ; npm run build ; npm run build:dsu ; npm run publish:dsu)
(cd egov/backoffice-dsu/egov-dapp ; npm run build ; npm run build:dsu ; npm run publish:dsu)
cd egov
