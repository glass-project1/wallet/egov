#!/bin/bash -xe
# Tool to reset database in egov
MY_PWD=${PWD##*/}          # to assign to a variable
MY_PWD=${MY_PWD:-/}        # to correct for the case where PWD=/
if [ "$MY_PWD" != egov ]
then
    echo 1>&2 "Current working dir must be egov"
    exit 1
fi
cd backoffice-sql
PGPASSWORD=egov psql -h localhost egov egov <<EOF
DROP OWNED BY egov;
\i lib/sql/install/egov.sql
EOF

