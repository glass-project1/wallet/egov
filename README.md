# eGov


This project is a placeholder to a system for demo of the Ministry of Digital Governance (MoDJ).

It manages the creation of EGovDApps. (Contains a legacy database, that records all issued EGovDApps, and a web interface - to be operated by an MoDJ employee - to issue those EGovDApps).

It is to be based on https://gitlab.com/glass-project1/wallet/egov-id

## Workspace Setup for Development

Please see the README.md of each folder (in sequence).

### backoffice-sql

The backoffice SQL database setup files, with initial "specimen data".
This database contains a record of all issued wallets.

### backoffice-dsu

OpenDSU services and a web-application REST services gateway.

### backoffice-frontend

A web frontend application (that also uses the REST services to access the database).

## Production

The docker image details are defined by the `Docker-ci` file (made to run on gitlab.com).

The deployment in the production environment is handled by the glass-wallet-workspace project.


  
